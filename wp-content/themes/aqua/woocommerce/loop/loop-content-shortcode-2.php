<?php global $product;
	$tb_options = $GLOBALS['tb_options'];
	$show_sale_flash = (int) isset( $show_sale_flash ) ? $show_sale_flash : $tb_options['tb_archive_show_sale_flash_product'];
	$show_quick_view = (int) isset( $show_quick_view ) ? $show_quick_view : $tb_options['tb_archive_show_quick_view_product'];
	$show_rating = (int) isset( $show_rating ) ? $show_rating : $tb_options['tb_archive_show_rating_product'];
	$show_add_to_cart = (int) isset( $show_add_to_cart ) ? $show_add_to_cart : $tb_options['tb_archive_show_add_to_cart_product'];
	$show_whishlist = (int) isset( $show_whishlist ) ? $show_whishlist : $tb_options['tb_archive_show_whishlist_product'];
	$show_title = (int) isset( $show_title ) ? $show_title : $tb_options['tb_archive_show_title_product'];
	$show_price = (int) isset( $show_price ) ? $show_price : $tb_options['tb_archive_show_price_product'];
	//$width_image = isset( $width_image ) ? $width_image : 300;
	//$height_image = isset( $height_image ) ? $height_image : 200;
	//$crop_image = isset( $crop_image ) ? $crop_image : 1;
?>
<div <?php post_class(); ?>>
							
	<div class="tb-product-item-inner">
		
		<div class="tb-image">
			<?php if( $show_sale_flash ) do_action( 'woocommerce_template_product_loop_sale_flash' ); ?>
			<?php
				$postDate = strtotime( get_the_date('j F Y') );
				$todaysDate = time() - (7 * 86400);// publish < current time 1 week
				if( $postDate >= $todaysDate) echo '<span class="new">'. esc_html__('New', 'aqua') .'</span>';
				$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false);
				$image_full = $attachment_image[0];
				//echo $crop_image .' - '. $width_image .' - '. $height_image;
				if($crop_image){
					$image_resize = matthewruddy_image_resize( $attachment_image[0], $width_image, $height_image, true, false );
					echo '<img style="width:100%;" class="bt-image-cropped" src="'. esc_attr($image_resize['url']) .'" alt="">';
				}else{
					the_post_thumbnail();
				}
			?>
		</div>
		
		<div class="tb-content">
			
			<div class="tb-footer-content">
				<?php 
					if ( $product->is_in_stock() ) {
						echo '<div class="stock" >' . __(' Availability ', 'aqua' ) . $product->get_stock_quantity() . '<span>' . __( ' in stock', 'aqua' ) . '</span></div>';
					} else {
						echo '<div class="out-of-stock" >' . __( 'out of stock', 'aqua' ) . '</div>';
					}
				?>
				<?php if( $show_title ): ?>
				<div class="tb-title text-ellipsis"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
				<?php endif; ?>
				<div class="tb-price-rating">
					<?php
						if( $show_price ) do_action( 'woocommerce_template_product_loop_price' );
					?>
				</div>
			</div>
			<div class="tb-header-content">
				
				<div class="tb-action">
					<?php
						if( $show_add_to_cart ):
					?>
					<div class="tb-product-btn tb-btn-tocart">
						<?php do_action( 'woocommerce_template_loop_add_to_cart' ); ?>
					</div>
					<?php endif;
					if( $show_whishlist ):
						if( function_exists('YITH_WCWL') ):
							$wishlist_text = YITH_WCWL()->is_product_in_wishlist( get_the_ID() ) ? __(' Browse Wishlist','aqua') : __('Add To Wishlist', 'aqua');
					 ?>
					<div class="tb-product-btn tb-btn-wishlist">
						<?php echo do_shortcode('[yith_wcwl_add_to_wishlist]');?>
					</div>
					<?php
						endif;
					endif;
					if( $show_compare ):
					?>
					<div class="tb-product-btn tb-btn-compare">
						<div title="<?php _e('Add To Compare','aqua');?>">
							<?php tb_add_compare_link();?>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	
</div>