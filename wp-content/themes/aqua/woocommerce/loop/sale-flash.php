<?php
/**
 * Product loop sale flash
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;
	//product hot
	$featured_value = get_post_meta( $product->get_id(), '_featured', true );
	if ( $featured_value == 'yes' ) {
		echo '<span class="onsale hot">'.__('Hot', 'preshool').'</span>';
	} else {
		//product discount
		if ( $product->is_on_sale() &&  $product->is_type( 'simple' )) {
			$percentage = round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 );
			echo '<span class="onsale"> - '.$percentage.'%</span>';
		}
		//product sale
		if ( ($product->is_on_sale() && $product->is_type( 'variable' )) || ($product->is_on_sale() &&  $product->is_type('external') )) {
			echo '<span class="onsale sale_variable">'.__('Sale', 'preshool').'</span>';
		}
	}
?>