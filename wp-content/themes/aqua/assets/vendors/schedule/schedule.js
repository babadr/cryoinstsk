(function ($) {
    "use strict"; 
    // Remove redundant cell when using rowspan
    $(document).ready(function() {
          $('table tr').each(function(i, v) {
            var cols = $(this).find('td');
            $(this).find('td[rowspan]').each(function() {
              var idx = $(this).index();
              for(var j = 1; j < $(this).prop('rowspan'); j++) {
                $('table tr').eq(i+j).find('td').eq(idx).hide();
              }
            });
          });
    });
})(jQuery);