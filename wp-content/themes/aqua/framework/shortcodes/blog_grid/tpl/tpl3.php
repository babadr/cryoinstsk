<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="ro-image">
		 <?php
			
			$image_full = '';
			$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false);
		
			$image_full = $attachment_image[0];
			if($crop_image && has_post_thumbnail()){
				$image_resize = matthewruddy_image_resize( $attachment_image[0], $width_image, $height_image, true, false );
				echo '<img style="width:100%;" class="bt-image-cropped" src="'. esc_attr($image_resize['url']) .'" alt="">';
			}else{
				the_post_thumbnail();
			}
			?>
	</div>
	<div class="ro-relate-posts-item">	
		<p class="tb-date"><?php echo '<span>'. get_the_date('d') .'</span>'. get_the_date('F'); ?></p>
		
		<div class="ro-content">
			<?php if($show_title) { ?>
				<h4><a class="ro-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			<?php } ?>
			<?php if($show_info) echo tb_theme_meta_blog_render();
			if($show_excerpt) { ?>
				<div class="tb-excerpt">
					<?php echo tb_custom_excerpt( intval( $excerpt_lenght ), $excerpt_more); ?>
				</div>
			<?php } ?>
		</div>
	</div>
</article>