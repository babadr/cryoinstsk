<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="ro-relate-posts-item">	
		<p class="tb-date"><?php echo '<span>'. get_the_date('d') .'</span>'. get_the_date('F'); ?></p>
		
		<div class="ro-content">
			<?php if($show_title) { ?>
				<h4><a class="ro-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			<?php } ?>
			<?php if($show_info) echo tb_theme_meta_blog_render();
			if($show_excerpt) { ?>
				<div class="tb-excerpt">
					<?php echo tb_custom_excerpt( intval( $excerpt_lenght ), $excerpt_more); ?>
				</div>
			<?php } ?>
		</div>
	</div>
</article>