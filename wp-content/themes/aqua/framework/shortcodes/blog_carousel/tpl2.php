<?php
$attachment_image1 = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false);
$image_resize1 = matthewruddy_image_resize( $attachment_image1[0], 100, 100, true, false );
?>

<li class="tb-item" data-thumb ="<?php echo esc_attr($image_resize1['url']); ?>">

	<div class="tb-item-content">
		<div class="tb-image">
			<?php $image_full = '';
			if(has_post_thumbnail()){
				$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false);
				$image_full = $attachment_image[0];
				if($crop_image){
					$image_resize = matthewruddy_image_resize( $attachment_image[0], $width_image, $height_image, true, false );
					echo '<img style="width:100%;" class="bt-image-cropped" src="'. esc_attr($image_resize['url']) .'" alt="">';
				}else{
					the_post_thumbnail();
				}
			}?>
		</div>
		<div class="tb-content-block">
			
			<?php if($show_title) echo tb_theme_title_render(); ?>
			<?php if($show_desc) echo '<div class="blog-desc">'.tb_custom_excerpt($excerpt_length , '').'<a href="'.get_permalink().'">'.$excerpt_more.'</a></div>'; ?>
			
		</div>
	</div>
</li>