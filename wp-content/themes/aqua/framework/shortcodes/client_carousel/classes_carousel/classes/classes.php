<?php
function tb_classes_func($atts) {
    extract(shortcode_atts(array(
		'tpl' => 'tpl1',
        'post_type' => 'classes',
        'posts_per_page' => -1,
        'classes_category' => '',
		'columns' =>  4,
        'crop_image' => 1,
        'show_popup' => 1,
        'width_image' => 530,
        'height_image' => 250,
        'show_title' => 1,
        'show_price' => 0,
        'show_info' => 1,
        'show_cate' => 1,
        'show_desc' => 0,
        'show_pagination' => 0,
		'pos_pagination' => 'text-center',
        'excerpt_length' => 5,
        'excerpt_more' => '',
        'orderby' => 'none',
        'order' => 'none',
		'ob_animation' => 'wrap',
		'animation' => '',
        'el_class' => ''
    ), $atts));
    
	$style_wrap = array();
	$class = array();
	
	$category = $classes_category;
	$taxonomy = 'classes_category';
           
    $cl_effect = getCSSAnimation($animation);
    
    $class[] = 'tb-classes';
    $class[] = $tpl;
 
	if($ob_animation == 'wrap') $class[] = $cl_effect;
    $class[] = $el_class;
    
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    
    $args = array(
        'posts_per_page' => $posts_per_page,
        'paged' => $paged,
        'orderby' => $orderby,
        'order' => $order,
        'post_type' => $post_type,
        'post_status' => 'publish');
    if (isset($category) && $category != '') {
        $cats = explode(',', $category);
        $category = array();
        foreach ((array) $cats as $cat) :
        $category[] = trim($cat);
        endforeach;
        $args['tax_query'] = array(
                                array(
                                    'taxonomy' => $taxonomy,
                                    'field' => 'id',
                                    'terms' => $category
                                )
                        );
    }
    $wp_query = new WP_Query($args);
    
    ob_start();
    if ( $wp_query->have_posts() ) {
    ?>
    <div class="row <?php echo esc_attr(implode(' ', $class)); ?>" style="<?php echo esc_attr(implode(' ', $style_wrap)); ?>">
		<?php
			$loop = 0;
			$class_columns = array();
			$columns = ($tpl == 'tpl1') ? 2 : $columns;
			
			switch ($columns) {
				case 1:
					$class_columns[] = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
					break;
				case 2:
					$class_columns[] = 'col-xs-12 col-sm-6 col-md-6 col-lg-6';
					break;
				case 3:
					$class_columns[] = 'col-xs-12 col-sm-6 col-md-4 col-lg-4';
					break;
				case 4:
					$class_columns[] = 'col-xs-12 col-sm-6 col-md-4 col-lg-3';
					break;
				default:
					$class_columns[] = 'col-xs-12 col-sm-6 col-md-4 col-lg-3';
					break;
			}
			
			if($ob_animation == 'item') $class_columns[] = $cl_effect;
			
			$i = 0;
			
			while ( $wp_query->have_posts() ) { $wp_query->the_post();
				$big_item  ='tb-classes-item';
				if($i == 0 & $tpl == 'tpl1'){
					$height_image = $height_image * 2 + 30;
					$big_item .= ' tb-big-item';
				}
				if($i == 1 & $tpl == 'tpl1') $height_image = ($height_image - 30) / 2;
				echo '<div class="'.esc_attr(implode(' ', $class_columns)).' '.$big_item.'">';
					//include $tpl.'.php';
					include $tpl .".php";	
				echo '</div>';
				$i++;
			}
		?>
		<div style="clear: both;"></div>
        <?php if($show_pagination){ ?>
			<nav class="pagination number <?php echo esc_attr($pos_pagination); ?>" role="navigation">
				<?php
					$big = 999999999; // need an unlikely integer

					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages,
						'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'aqua' ),
						'next_text' => __( '<i class="fa fa-angle-right"></i>', 'aqua' ),
					) );
				?>
			</nav>
		<?php } ?>
    </div>
    <?php
    }else {
            echo 'Post not found!';
    } 
    ?>
    
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}

if(function_exists('insert_shortcode')) {
	insert_shortcode('tb_classes', 'tb_classes_func');
}
