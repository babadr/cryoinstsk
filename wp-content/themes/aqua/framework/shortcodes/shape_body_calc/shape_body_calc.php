<?php
function ro_shape_body_calc_func($atts, $content = null) {
    extract(shortcode_atts(array(
		'tpl'  => 'tpl1',
		'title' => '',
		'subtitle' =>'',
        'el_class' => '',
    ), $atts));
			
    $class = array();
    $class[] = 'ro-shape-body-calculator';
    $class[] = $el_class;
    $class[] = $tpl;
	wp_enqueue_script('cal_body.page.ajax', URI_PATH_FR . '/shortcodes/shape_body_calc/ajax-page.js');
	wp_localize_script( 'cal_body.page.ajax', 'variable_js', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
	));
    ob_start();
    ?>
	<div class="<?php echo esc_attr(implode(' ', $class)); ?>">
		<?php if ($tpl != 'tpl3'){ ?>
		<div class="wbp-title">
			<h3><?php echo tb_filtercontent($title);?></h3>
			<p><?php echo tb_filtercontent($subtitle);?></p>
		</div>
		<form name="form" action="/home/body-type-calculator/" method="get">
			<input type="text" name="bust" id="bust" placeholder="<?php echo __("Bust (cm)","aqua");?>">
			<input type="text" name="waist" id="waist" placeholder="<?php echo __("Waist (cm)","aqua");?>">
			<input type="text" name="hips" id="hips" placeholder="<?php echo __("Hips (cm)","aqua");?>">
			<input type="submit" name="submit" value="<?php echo __("Calculator Now","aqua");?>" class="btn-calc-trans" >
		</form>
		
		<?php } else include 'tpl3.php'; ?>
		<div style="clear: both"></div>
	</div>
    <?php
    return ob_get_clean();
}

if(function_exists('insert_shortcode')) { insert_shortcode('shape_calculator', 'ro_shape_body_calc_func'); }
