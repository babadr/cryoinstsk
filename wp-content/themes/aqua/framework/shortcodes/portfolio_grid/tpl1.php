<?php

	$lists_thumb = array('portfolio-vertical-thumb'=>array(0,3),'portfolio-normal-thumb'=>array(1,2),'portfolio-horizontal-thumb'=> array(4));
	
	while( $p->have_posts() ) :  $p->the_post();
	$terms = wp_get_post_terms(get_the_ID(), 'portfolio_category');

		if ( !empty( $terms ) && !is_wp_error( $terms ) ){

			$term_list = array();

			foreach ( $terms as $term ) {
				$term_list[] = $term->slug;
			}

		}
		$j = $i % 5;
		$thumb = 'portfolio-normal-thumb';
		foreach( $lists_thumb as $k=>$thumbs ){
			
			if( array_search( $j, $thumbs) !== false ){
				$thumb = $k;
				break;
			}
		}
		$width = tb_get_image_width( $thumb );
		//echo $width;
		//echo $thumb;
		
		
		$full = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ),'full' );
		if( $full ):
		?>
		<div class="grid-item <?php echo esc_attr($thumb);?>">
		
			<div class="tb-content" style ="display: none;">
				<?php
				if($show_title): ?> 
					<h2> <?php the_title(); ?> </h2>
				<?php endif; ?>
				<a href="#" class="tb-open-lighth-box" data-src="<?php echo esc_url( $full[0] );?>">
					<i class="ion-ios-plus-empty"></i>
				</a>
			</div>
			<?php the_post_thumbnail( $thumb ); ?>
		</div>
		<?php
		$i++;
		endif;
	endwhile;
 ?>