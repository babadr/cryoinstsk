<?php 
	$sty_image = wp_get_attachment_image_src($bg_image, 'full', false);
?>
<div class="tb-pricing <?php echo esc_attr(implode(' ', $class)); ?>" style="background-image: url('<?php echo esc_url($sty_image[0]); ?>')">
	<div class="tb-pricing-content" style="background-color: <?php echo esc_attr($color); ?>">

		<?php if(!empty($title)): ?><h4><?php echo tb_filtercontent($title);?></h4>
		<?php endif;if( ! empty( $content ) ) echo $content;?>

	</div>
</div>