<?php 
	$sty_image = wp_get_attachment_image_src($bg_image, 'full', false);
?>
<div class="tb-pricing <?php echo esc_attr(implode(' ', $class)); ?>">
	<div class="tb-pricing-content text-center">
	<div class="tb-pricing-top" style="background-image: url('<?php echo esc_url($sty_image[0]); ?>')">
		<?php if(!empty($title)): ?><h4><?php echo tb_filtercontent($title);?></h4>
		<?php endif; if(!empty($sub_title)):?>
		<p class="sub_title"><?php echo tb_filtercontent($sub_title);?></p>
		<p class="t-price"><?php echo tb_filtercontent($price);?></p>
	</div>
		<?php endif; if( ! empty( $content ) ) echo '<div class="pricing-content">'.$content.'</div>';
		if(!empty($btn_text)): 	?>
		<a class="btn-button" href="<?php echo esc_url($ex_link); ?>"><?php echo esc_html($btn_text);?></a>
		<?php endif; ?>
		<div style="clear: both"></div>
	</div>
</div>