<div class="ro-service-item-11 clearfix">
	<div class="tb-icon">
		<?php echo wp_get_attachment_image($icon, 'full'); ?>
		<?php echo wp_get_attachment_image($icon_active, 'full'); ?>
	</div>
	<div class="tb-content">
		<?php echo '<p class="sub_title">'.esc_html($subtitle).'</p>' ?>
		<?php echo '<h4>'.esc_html($title).'</h4>' ?>
	</div>
</div>