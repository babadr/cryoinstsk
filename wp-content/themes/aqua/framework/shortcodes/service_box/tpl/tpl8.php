<div class="ro-service-item-8">
	<div>
		<?php echo wp_get_attachment_image($image, 'full'); ?>
		<?php echo '<h4>'.esc_html($title).'</h4>' ?>
		<?php echo '<p class="sub_title">'.esc_html($subtitle).'</p>' ?>
		<?php echo '<p>'.$desc.'</p>'; ?>
		<a class="btn-button" href="<?php echo esc_url($ex_link); ?>"><?php echo esc_html($txt_button);?></a>
	</div>
</div>