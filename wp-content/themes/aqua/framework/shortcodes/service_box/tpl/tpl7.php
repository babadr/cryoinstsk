<div class="tb-service tb-incremental">
	<?php
		$max = intval( $title );
		if(!empty($image)){?>
		<div class="tb-icon">
			<?php echo wp_get_attachment_image($image, 'full'); ?>
		</div>
		<?php }
		if( ! empty( $title ) ) echo '<div class="tb-countup-content"><h3 class="tb-title"><span class="counter">'. $max .'</span>'. str_replace( $max, '', $title ) .'</h3>';
		if( ! empty( $desc ) ) echo '<p>'. esc_html($desc) .'</p></div>';
	?>
</div>