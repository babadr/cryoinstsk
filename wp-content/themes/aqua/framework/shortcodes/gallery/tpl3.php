<?php
	while( $p->have_posts() ) :  $p->the_post();
		$terms = wp_get_post_terms(get_the_ID(), 'gallery_category');

		if ( !empty( $terms ) && !is_wp_error( $terms ) ){

			$term_list = array();

			foreach ( $terms as $term ) {

				$term_list[] = $term->slug;

			}

		}

		$full = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ),'full' );
		if( $full ):
			?>
			<div class="grid-item" style="width:<?php echo (100 / $posts_per_page);?>%">
				<?php the_post_thumbnail('gallery_small_thumb_2'); ?>
			</div>
			<?php
		endif;
	endwhile;
 ?>