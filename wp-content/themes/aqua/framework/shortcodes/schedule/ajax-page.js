!(function($){
	$(function(){
		var $schedule_filter = $('.schedule-filter');
		$schedule_filter.each(function(){
			var $pagination = $(this);
			$pagination.on('click', 'a', function(e){
				e.preventDefault();
				
				var _table_schedule = $(this).closest('.schedule-container');
				if( _table_schedule.length === 0 ) return;
				
				var params = {},
					_this = $(this);
					
				params.datestart = _table_schedule.find('#tb_date_start').val();
				params.dateend = _table_schedule.find('#tb_date_end').val();
				params.args = _table_schedule.find('.table-args').data('args');
				params.atts = _table_schedule.find('.table-args').data('atts');
				_table_responsive = _table_schedule.find('.table-responsive');
				_table_responsive.addClass('blog-more-ajax-loading');
				$.ajax({
					type: "POST",
					url: variable_js.ajax_url,
					data: {action: 'render_schedule_table', params: params},
					success: function(data){
						_table_responsive.removeClass('blog-more-ajax-loading');
						_table_responsive.find('.table-schedule').empty().append(data);
					}
				})
			})
		})
	})
})(jQuery) 