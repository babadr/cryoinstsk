<?php

// Register Custom Post Type

function jws_theme_add_post_type_spa() {

    // Register taxonomy

    $labels = array(

            'name'              => _x( 'Spa Category', 'taxonomy general name', 'aqua' ),

            'singular_name'     => _x( 'Spa Category', 'taxonomy singular name', 'aqua' ),

            'search_items'      => __( 'Search Spa Category', 'aqua' ),

            'all_items'         => __( 'All Spa Category', 'aqua' ),

            'parent_item'       => __( 'Parent Spa Category', 'aqua' ),

            'parent_item_colon' => __( 'Parent Spa Category:', 'aqua' ),

            'edit_item'         => __( 'Edit Spa Category', 'aqua' ),

            'update_item'       => __( 'Update Spa Category', 'aqua' ),

            'add_new_item'      => __( 'Add New Spa Category', 'aqua' ),

            'new_item_name'     => __( 'New Spa Category Name', 'aqua' ),

            'menu_name'         => __( 'Spa Category', 'aqua' ),

    );



    $args = array(

            'hierarchical'      => true,

            'labels'            => $labels,

            'show_ui'           => true,

            'show_admin_column' => true,

            'query_var'         => true,

            'rewrite'           => array( 'slug' => 'spa_category' ),

    );

    if(function_exists('custom_reg_taxonomy')) {

        custom_reg_taxonomy( 'spa_category', array( 'spa' ), $args );

    }
  

    //Register post type spa

    $labels = array(

            'name'                => _x( 'Spa', 'Post Type General Name', 'aqua' ),

            'singular_name'       => _x( 'Spa Item', 'Post Type Singular Name', 'aqua' ),

            'menu_name'           => __( 'SPA', 'aqua' ),

            'parent_item_colon'   => __( 'Parent Item:', 'aqua' ),

            'all_items'           => __( 'All Items', 'aqua' ),

            'view_item'           => __( 'View Item', 'aqua' ),

            'add_new_item'        => __( 'Add New Item', 'aqua' ),

            'add_new'             => __( 'Add New', 'aqua' ),

            'edit_item'           => __( 'Edit Item', 'aqua' ),

            'update_item'         => __( 'Update Item', 'aqua' ),

            'search_items'        => __( 'Search Item', 'aqua' ),

            'not_found'           => __( 'Not found', 'aqua' ),

            'not_found_in_trash'  => __( 'Not found in Trash', 'aqua' ),

    );

    $args = array(

            'label'               => __( 'Spa', 'aqua' ),

            'description'         => __( 'Spa Description', 'aqua' ),

            'labels'              => $labels,

            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),

            'taxonomies'          => array( 'spa_category', 'spa_tag' ),

            'hierarchical'        => true,

            'public'              => true,

            'show_ui'             => true,

            'show_in_menu'        => true,

            'show_in_nav_menus'   => true,

            'show_in_admin_bar'   => true,

            'menu_position'       => 5,

            'menu_icon'           => 'dashicons-smiley',

            'can_export'          => true,

            'has_archive'         => true,

            'exclude_from_search' => false,

            'publicly_queryable'  => true,

            'capability_type'     => 'page',

    );

    

    if(function_exists('custom_reg_post_type')) {

        custom_reg_post_type( 'spa', $args );

    }

    

}



// Hook into the 'init' action

add_action( 'init', 'jws_theme_add_post_type_spa', 0 );

