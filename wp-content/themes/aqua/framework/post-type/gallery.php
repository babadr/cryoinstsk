<?php
	// Register Custom Post Type
	function tb_add_post_type_gallery() {
		// Register taxonomy
		$labels = array(
		'name'              => _x( 'Gallery Category', 'taxonomy general name', 'aqua' ),
		'singular_name'     => _x( 'Gallery Category', 'taxonomy singular name', 'aqua' ),
		'search_items'      => __( 'Search gallery Category', 'aqua' ),
		'all_items'         => __( 'All Gallery Category', 'aqua' ),
		'parent_item'       => __( 'Parent Gallery Category', 'aqua' ),
		'parent_item_colon' => __( 'Parent Gallery Category:', 'aqua' ),
		'edit_item'         => __( 'Edit Gallery Category', 'aqua' ),
		'update_item'       => __( 'Update Gallery Category', 'aqua' ),
		'add_new_item'      => __( 'Add New Gallery Category', 'aqua' ),
		'new_item_name'     => __( 'New Gallery Category Name', 'aqua' ),
		'menu_name'         => __( 'Gallery Category', 'aqua' ),
		);
		$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'gallery_category' ),
		);
		if(function_exists('custom_reg_taxonomy')) {
			custom_reg_taxonomy( 'gallery_category', array( 'gallery' ), $args );
		}
		//Register post type gallery
		$labels = array(
		'name'                => _x( 'Gallery', 'Post Type General Name', 'aqua' ),
		'singular_name'       => _x( 'Gallery Item', 'Post Type Singular Name', 'aqua' ),
		'menu_name'           => __( 'Gallery', 'aqua' ),
		'parent_item_colon'   => __( 'Parent Item:', 'aqua' ),
		'all_items'           => __( 'All Items', 'aqua' ),
		'view_item'           => __( 'View Item', 'aqua' ),
		'add_new_item'        => __( 'Add New Item', 'aqua' ),
		'add_new'             => __( 'Add New', 'aqua' ),
		'edit_item'           => __( 'Edit Item', 'aqua' ),
		'update_item'         => __( 'Update Item', 'aqua' ),
		'search_items'        => __( 'Search Item', 'aqua' ),
		'not_found'           => __( 'Not found', 'aqua' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'aqua' ),
		);
		$args = array(
		'label'               => __( 'Gallery', 'aqua' ),
		'description'         => __( 'Gallery Description', 'aqua' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'gallery_category', 'gallery_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-images-alt2',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
		if(function_exists('custom_reg_post_type')) {
			custom_reg_post_type( 'gallery', $args );
		}
	}
	// Hook into the 'init' action
add_action( 'init', 'tb_add_post_type_gallery', 10 );