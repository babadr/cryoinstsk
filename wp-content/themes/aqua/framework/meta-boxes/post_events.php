<div id="jws_theme_events_metabox" class='tb-events-metabox'>
	<?php
	$this->text('time',
			'Event time',
			'',
			__('Enter event time for this events post.','aqua')
	);
	?>
	
	<?php
	$this->text('address',
			'Event address',
			'',
			__('Enter event address for this events post.','aqua')
	);
	?>

</div>