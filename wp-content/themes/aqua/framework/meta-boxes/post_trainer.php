<div id="jws_theme_trainer_metabox" class='tb-trainer-metabox'>
	<?php
	$this->text('trainer_position',
		'Position',
		'',
		__('Enter position of member.','aqua')
	);
	?>
	<?php
	$this->text('phone',
		'Phone',
		'',
		__('Enter phone number for this trainner post.','aqua')
	);
	?>
	<?php
	$this->text('address',
		'Address',
		'',
		__('Enter address for this trainner post.','aqua')
	);
	?>
	<?php
	$this->text('start_time',
		'Start time',
		'',
		__('Enter start time for this trainner post.','aqua')
	);
	?>
	<?php
	$this->text('end_time',
		'End time',
		'',
		__('Enter end time for this trainner post.','aqua')
	);
	?>
	<?php
	$this->text('email',
		'Email',
		'',
		__('Enter email for this trainner post.','aqua')
	);
	?>
	<?php
	$this->textarea('trainer_social',
		'Socials',
		'',
		__('Enter social of member.','aqua')
	);
	?>
</div>