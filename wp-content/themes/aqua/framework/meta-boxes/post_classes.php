<div id="jws_theme_classes_metabox" class='tb-classes-metabox'>
	<?php
	$this->input_date('date',
			'Date',
			'',
			__('Enter date for this classes post.','aqua')
	);
	?>
	
	<?php
	$this->select('start_time',
			'Start time',
			array(
				'7' => '07:00',
				'8' => '08:00',
				'9' => '09:00',
				'10' => '10:00',
				'11' => '11:00',
				'12' => '12:00',
				'13' => '13:00',
				'14' => '14:00',
				'15' => '15:00',
				'16' => '16:00',
				'17' => '17:00',
				'18' => '18:00',
				'19' => '19:00',
				'20' => '20:00',
			),
			'',
			__('Enter start time for this classes post.','aqua')
	);
	?>
	
	
	<?php
	$this->select('end_time',
			'End time',
			array(
				'7' => '07:00',
				'8' => '08:00',
				'9' => '09:00',
				'10' => '10:00',
				'11' => '11:00',
				'12' => '12:00',
				'13' => '13:00',
				'14' => '14:00',
				'15' => '15:00',
				'16' => '16:00',
				'17' => '17:00',
				'18' => '18:00',
				'19' => '19:00',
				'20' => '20:00',
			),
			'',
			__('Enter end time for this classes post.','aqua')
	);
	?>
	
	
	<?php
	$this->text('level',
			'Level Classes',
			'',
			__('Enter level classes for this classes post.','aqua')
	);
	?>
	
	<?php
		$this->text('price',
				'Price per 30 days',
				'',
				__('Enter price for this classes post.','aqua')
		);
	?>
	<?php
	$this->multiple('day_of_week',
			'Day of week',
			array(
				'monday' => 'Monday',
				'tuesday' => 'Tuesday',
				'wednesday' => 'Wednesday',
				'thurday' => 'Thurday',
				'friday' => 'Friday',
				'saturday' => 'Saturday',
				'sunday' => 'Sunday',
			),
			'',
			__('Check days for this classes.','aqua')
	);?>

	<?php
	$this->selectTrainer('trainer','Trainer', __('Select a trainer for this classes.','aqua'));?>
	
	
</div>