<?php
class TB_Trainner_Widget extends RO_Widget {
	public function __construct() {
		$this->widget_cssclass    = 'tb-post tb-widget-trainer-list';
		$this->widget_description = __( 'Display a list of your posts on your site.', 'aqua' );
		$this->widget_id          = 'tb_trainer_list';
		$this->widget_name        = __( 'TB Trainer Widget', 'aqua' );
		$this->settings           = array(
			'title'  => array(
				'type'  => 'text',
				'std'   => __( 'Post List', 'aqua' ),
				'label' => __( 'Title', 'aqua' )
			),
			'sub_title'  => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Sub Title', 'aqua' )
			),
			'posts_per_page' => array(
				'type'  => 'number',
				'step'  => 1,
				'min'   => 1,
				'max'   => '',
				'std'   => 3,
				'label' => __( 'Number of posts to show', 'aqua' )
			),
			'orderby' => array(
				'type'  => 'select',
				'std'   => 'date',
				'label' => __( 'Order by', 'aqua' ),
				'options' => array(
					'none'   => __( 'None', 'aqua' ),
					'title'  => __( 'Title', 'aqua' ),
					'date'   => __( 'Date', 'aqua' ),
					'ID'  => __( 'ID', 'aqua' ),
				)
			),
			'order' => array(
				'type'  => 'select',
				'std'   => 'none',
				'label' => __( 'Order', 'aqua' ),
				'options' => array(
					'none'  => __( 'None', 'aqua' ),
					'asc'  => __( 'ASC', 'aqua' ),
					'desc' => __( 'DESC', 'aqua' ),
				)
			),
			'el_class'  => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Extra Class', 'aqua' )
			)
		);
		parent::__construct();
		add_action('admin_enqueue_scripts', array($this, 'widget_scripts'));
	}
        
	public function widget_scripts() {
		wp_enqueue_script('widget_scripts', URI_PATH . '/framework/widgets/widgets.js');
	}

	public function widget( $args, $instance ) {
		
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();
		extract( $args );
                
		$title                  = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		$posts_per_page         = absint( $instance['posts_per_page'] );
		$orderby                = sanitize_title( $instance['orderby'] );
		$order                  = sanitize_title( $instance['order'] );
		$el_class               = sanitize_title( $instance['el_class'] );
        $sub_title               = sanitize_title( $instance['sub_title'] );

		
		
                echo tb_filtercontent($before_widget);

                if ( $title )
                        echo tb_filtercontent($before_title . $title . $after_title);
                if($sub_title) echo '<span class="subtitle">'. tb_filtercontent($sub_title). '</span>';
                $query_args = array(
                    'posts_per_page' => $posts_per_page,
                    'orderby' => $orderby,
                    'order' => $order,
                    'post_type' => 'trainer',
                    'post_status' => 'publish');
                
                $wp_query = new WP_Query($query_args);
				
				if ($wp_query->have_posts()){
					
					
					?>
					<div class="tb-trainer-list">
						<?php while ($wp_query->have_posts()){ $wp_query->the_post();
							$start_time = get_post_meta(get_the_ID(), 'tb_start_time', true);
							$end_time = get_post_meta(get_the_ID(), 'tb_end_time', true);
							$tb_phone = get_post_meta(get_the_ID(), 'tb_phone', true);
							$tb_email = get_post_meta(get_the_ID(), 'tb_email', true);
							$tb_position = get_post_meta(get_the_ID(), 'tb_trainer_position', true);
						?>
							<div class="tb-trainer-side">
								<div class="tb-trainer-img">
									<a class="trainer-featured-img" href="<?php the_permalink(); ?>">
										<?php
										if(has_post_thumbnail()){
											//the_post_thumbnail('thumbnail');
											$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false);
											$image_full = $attachment_image[0];
											if($image_full){
												$image_resize = matthewruddy_image_resize( $attachment_image[0], 270, 266, true, false );
												echo '<img style="width:100%;" class="bt-image-cropped" src="'. esc_attr($image_resize['url']) .'" alt="">';
											}else{
												the_post_thumbnail();
											}
										}else{
											echo '<img alt="Image-Default" class="attachment-thumbnail wp-post-image" src="'. esc_attr(URI_PATH.'/assets/images/post_default.jpg') .'">';
										}
										?>
									</a>
								</div>
								<div class="tb-trainer-side-ct text-center">
									<?php if($tb_phone){ ?>
									<div class="tb-phone"><?php echo _e('Phone: ','aqua');?>	
										<span><?php echo esc_attr($tb_phone); ?></span>
									</div>
									<?php } if($tb_email){ ?>
									<div class="tb-email"><?php echo _e('Email: ','aqua');?>
										<span><?php echo esc_attr($tb_email); ?></span>
									</div>
									<?php } if($start_time || $end_time){ ?>
									<div class="tb-time"><?php echo _e('Time: ','aqua');?>
										<span><?php echo esc_attr($start_time) .' - '. esc_attr($end_time); ?></span>
									</div>
									<?php } if($tb_position){ ?>
									<div class="tb-position">
										<span><?php echo esc_attr($tb_position); ?></span>
									</div>
									<?php } ?>
									<div class="tb-title">
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</div>
								</div>
							</div>
						<?php } ?>
						
					</div>
				<?php 
				}
				
                wp_reset_postdata();

                echo tb_filtercontent($after_widget);
                
		$content = ob_get_clean();

		echo tb_filtercontent($content);

		$this->cache_widget( $args, $content );
	}
}
/* Class TB_Post_List_Widget */
function register_trainner_list_widget() {
    register_widget('TB_Trainner_Widget');
}

add_action('widgets_init', 'register_trainner_list_widget');
