<?php
class Jws_Instagram_Widget extends WP_Widget { 
    function __construct() {
        parent::__construct(
                'jws_theme_instagram_widget', // Base ID
                __('Instagram', 'aqua'), // Name
                array('description' => __('Instagram Widget', 'aqua'),) // Args
        );
    }
	function widget($args, $instance) {
		extract($args);
		$title = !empty($instance['title']) ? $instance['title'] : '';
		$extra_class = !empty($instance['extra_class']) ? $instance['extra_class'] : '';
		$item = isset($instance['item']) ? esc_attr($instance['item']) : '';
		$limit = isset($instance['limit']) ? esc_attr($instance['limit']) : '';
		// no 'class' attribute - add one with the value of width
        if (strpos($before_widget, 'class') === false) {
            $before_widget = str_replace('>', 'class="' . $extra_class . '"', $before_widget);
        }
        // there is 'class' attribute - append width value to it
        else {
            $before_widget = str_replace('class="', 'class="' . $extra_class . ' ', $before_widget);
        }
		ob_start();
        echo tb_filtercontent($before_widget);
		
		if ( $title ) echo tb_filtercontent($before_title . $title . $after_title);
		
		?>
		
			<?php 
				function get_instagram($item = 3229242490,$limit=12){

					//$url = 'https://api.instagram.com/v1/users/'.$item.'/media/recent/?access_token=2912979.87fdd31.0949d22f4a714349ae84643c5af165ef&count='.$limit;
					
					$url = 'https://api.instagram.com/v1/users/'.$item.'/media/recent/?access_token=3229242490.1677ed0.a9c3a481214f4c16a3f0af96b513c3d9&count='.$limit;
					
					// Also Perhaps you should cache the results as the instagram API is slow
					$cache = './'.sha1($url).'.json';
					if(file_exists($cache) && filemtime($cache) > time() - 60*60){
						// If a cache file exists, and it is newer than 1 hour, use it
						$jsonData = json_decode(file_get_contents($cache));
					} else {
						$jsonData = json_decode((file_get_contents($url)));
						file_put_contents($cache,json_encode($jsonData));
					}
					$result = '<div id="instagram_feed" class="instagram_feed"><ul>'.PHP_EOL;
					foreach ($jsonData->data as $key=>$value) {
						$result .= "\t".'<li><a class="fancybox" target="_blank" href="'.$value->link .'">
										  <img src="'.$value->images->low_resolution->url.'" >
										  </a></li>'.PHP_EOL;
					}
					$result .= '</ul></div>'.PHP_EOL;
					return $result;
				}
				echo get_instagram($item,$limit);
			?>
		<?php
		
		echo tb_filtercontent($after_widget);
        echo ob_get_clean();
    }
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['limit'] = $new_instance['limit'];
		$instance['item'] = $new_instance['item'];
		$instance['extra_class'] = $new_instance['extra_class'];
        return $instance;
    }
    function form($instance) {
		$title = isset($instance['title']) ? esc_attr($instance['title']) : __('Recent Work', 'aqua');
		$item = isset($instance['item']) ? esc_attr($instance['item']) : '2713900256';
		$limit = isset($instance['limit']) ? esc_attr($instance['limit']) : '12';
		$extra_class = isset($instance['extra_class']) ? esc_attr($instance['extra_class']) : '';
		?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'aqua');?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('item')); ?>"><?php _e('UserId', 'aqua');?></label>
			<a href="<?php echo esc_url("http://jelled.com/instagram/lookup-user-id"); ?>"><?php _e('Get Your Instagram ID', 'aqua');?></a>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('item')); ?>" name="<?php echo esc_attr($this->get_field_name('item')); ?>" type="text" value="<?php echo esc_attr($item); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('limit')); ?>"><?php _e('Limit:', 'aqua');?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('limit')); ?>" name="<?php echo esc_attr($this->get_field_name('limit')); ?>" type="text" value="<?php echo esc_attr($limit); ?>" />
		</p>
		<p>
            <label for="<?php echo esc_attr($this->get_field_id('extra_class')); ?>"><?php _e('Extra Class:', 'aqua'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('extra_class')); ?>" name="<?php echo esc_attr($this->get_field_name('extra_class')); ?>" value="<?php echo tb_filtercontent($extra_class); ?>" />
        </p>
		<?php
    }
}
/**
 * Class TB_Recent_Work_Widget
 */
function register_instagram_widget() {
    register_widget('Jws_Instagram_Widget');
}
add_action('widgets_init', 'register_instagram_widget');
?>