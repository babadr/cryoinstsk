<?php

add_action('init', 'tb_blog_carousel_integrateWithVC');

function tb_blog_carousel_integrateWithVC() {
    vc_map(array(
        "name" => __("Blog carousel", 'aqua'),
        "base" => "tb_blog_carousel",
        "class" => "tb-blog-carousel",
        "category" => __('Aqua', 'aqua'),
        'admin_enqueue_js' => array(URI_PATH_ADMIN.'assets/js/customvc.js'),
        "icon" => "tb-icon-for-vc",
        "params" => array(
			array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Template", 'aqua'),
                "param_name" => "tpl",
                "value" => array(
					__("Template 1",'aqua') => "tpl1",
					__("Template 2",'aqua') => "tpl2",
					__("Template 3 - Full width",'aqua') => "tpl3",
                ),
                "description" => __('Select template in this elment.', 'aqua')
            ),
			array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns", 'aqua'),
                "param_name" => "columns",
                "value" => array(
                    "4 Columns" => "4",
                    "3 Columns" => "3",
                    "2 Columns" => "2",
                    "1 Column" => "1",
                ),
				"dependency" => array(
					"element" => "tpl",
					"value" => "tpl3",
				),
				"description" => __('Select columns for classes.', 'aqua')
            ),
            array (
                "type" => "tb_taxonomy",
                "taxonomy" => "category",
                "heading" => __ ( "Categories", 'aqua' ),
                "param_name" => "category",
                "class" => "",
                "description" => __ ( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'aqua' )
            ),
           array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Post Count", 'aqua'),
                "param_name" => "posts_per_page",
                "value" => "",
				"description" => __('Please, enter number of post per page for blog. Show all: -1.', 'aqua')
            ),
            array(
                "type" => "checkbox", 
                "heading" => __('Crop image', 'aqua'),
                "param_name" => "crop_image",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
                "description" => __('Crop or not crop image on your Post.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Width image', 'aqua'),
                "param_name" => "width_image",
                "description" => __('Enter the width of image. Default: 300.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Height image', 'aqua'),
                "param_name" => "height_image",
                "description" => __('Enter the height of image. Default: 200.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Title', 'aqua'),
                "param_name" => "show_title",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
				"group" => __("Template", 'aqua'),
                "description" => __('Show or hide title of post on your blog.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Info', 'aqua'),
                "param_name" => "show_info",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
				"group" => __("Template", 'aqua'),
                "description" => __('Show or hide info of post on your blog.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Excerpt', 'aqua'),
                "param_name" => "show_desc",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
				"group" => __("Template", 'aqua'),
                "description" => __('Show or hide description of post on your blog.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Excerpt Length', 'aqua'),
                "param_name" => "excerpt_length",
                "value" => '',
				'std' => -1,
				"group" => __("Template", 'aqua'),
                "description" => __('The length of the excerpt, number of words to display. Set -1 show all words of excerpt.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Excerpt More', 'aqua'),
                "param_name" => "excerpt_more",
                "value" => "",
				"group" => __("Template", 'aqua'),
				"description" => __('Please enter excerpt more for blog.', 'aqua')
            ),
            array(
                "type" => "dropdown",
                "heading" => __('Order by', 'aqua'),
                "param_name" => "orderby",
                "value" => array(
                    "None" => "none",
                    "Title" => "title",
                    "Date" => "date",
                    "ID" => "ID"
                ),
                "description" => __('Order by ("none", "title", "date", "ID").', 'aqua')
            ),
            array(
                "type" => "dropdown",
                "heading" => __('Order', 'aqua'),
                "param_name" => "order",
                "value" => Array(
                    "None" => "none",
                    "ASC" => "ASC",
                    "DESC" => "DESC"
                ),
                "description" => __('Order ("None", "Asc", "Desc").', 'aqua')
            ),
			array(
                "type" => "dropdown",
                "heading" => __('Object Animation', 'aqua'),
                "param_name" => "ob_animation",
                "value" => Array(
                    "Wrap" => "wrap",
                    "Item" => "item"
                ),
                "description" => __('Select object animation', 'aqua')
            ),
			array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Animation", 'aqua'),
                "param_name" => "animation",
                "value" => array(
                    "No" => "",
                    "Top to bottom" => "top-to-bottom",
                    "Bottom to top" => "bottom-to-top",
                    "Left to right" => "left-to-right",
                    "Right to left" => "right-to-left",
                    "Appear from center" => "appear"
                ),
                "description" => __("Box Animation", 'aqua')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", 'aqua'),
                "param_name" => "el_class",
                "value" => "",
				"description" => __ ( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'aqua' )
            ),
        )
    ));
}
