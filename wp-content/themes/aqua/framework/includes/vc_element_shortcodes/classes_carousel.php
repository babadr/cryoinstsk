<?php
add_action('init', 'tb_classes_carousel_integrateWithVC');

function tb_classes_carousel_integrateWithVC() {
    vc_map(array(
        "name" => __("Classes carousel", 'aqua'),
        "base" => "tb_classes_carousel",
        "class" => "tb-classes-carousel",
        "category" => __('Aqua', 'aqua'),
        'admin_enqueue_js' => array(URI_PATH_ADMIN.'assets/js/customvc.js'),
        "icon" => "tb-icon-for-vc",
        "params" => array(
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Template", 'aqua'),
				"param_name" => "tpl",
				"value" => array(
					__("Template 1 ~ Yoga",'aqua') => "tpl1",
					__("Template 2 ~ Fitness",'aqua') => "tpl2"
				),
				"description" => __('Select template in this element.', 'aqua')
			),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Post Count", 'aqua'),
                "param_name" => "posts_per_page",
                "value" => "",
				"description" => __('Please, enter number of post per page for classes. Show all: -1.', 'aqua')
            ),
            array (
                "type" => "tb_taxonomy",
                "taxonomy" => "classes_category",
                "heading" => __ ( "Categories", 'aqua' ),
                "param_name" => "classes_category",
                "class" => "",
                "description" => __ ( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'aqua' )
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns", 'aqua'),
                "param_name" => "columns",
                "value" => array(
                    "4 Columns" => "4",
                    "3 Columns" => "3",
                    "2 Columns" => "2",
                    "1 Column" => "1",
                ),
				"description" => __('Select columns for classes.', 'aqua')
            ),
			
            array(
                "type" => "checkbox", 
                "heading" => __('Crop image', 'aqua'),
                "param_name" => "crop_image",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				"group" => __("Template", 'aqua'),
                "description" => __('Crop or not crop image on your Post.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Width image', 'aqua'),
                "param_name" => "width_image",
				"group" => __("Template", 'aqua'),
                "description" => __('Enter the width of image. Default: 300.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Height image', 'aqua'),
                "param_name" => "height_image",
				"group" => __("Template", 'aqua'),
                "description" => __('Enter the height of image. Default: 200.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Categories', 'aqua'),
                "param_name" => "show_cate",
				"group" => __("Template", 'aqua'),
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
				"dependency" => array(
					"element"=>"tpl",
					"value"=> 'tpl1',
				),
                "description" => __('Show or hide categories of post on your classes.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Price', 'aqua'),
                "param_name" => "show_price",
				"group" => __("Template", 'aqua'),
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
				'dependency' => array(
					'element' => 'tpl',
					'value' => 'tpl2',
				),
                "description" => __('Show or hide price of post on your classes.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Title', 'aqua'),
                "param_name" => "show_title",
				"group" => __("Template", 'aqua'),
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
                "description" => __('Show or hide title of post on your classes.', 'aqua')
            ),
			
            array(
                "type" => "checkbox",
                "heading" => __('Show Info', 'aqua'),
                "param_name" => "show_info",
				"group" => __("Template", 'aqua'),
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
                "description" => __('Show or hide info of post on your classes.', 'aqua')
            ),
           
            array(
                "type" => "dropdown",
                "heading" => __('Order by', 'aqua'),
                "param_name" => "orderby",
                "value" => array(
                    "None" => "none",
                    "Title" => "title",
                    "Date" => "date",
                    "ID" => "ID"
                ),
                "description" => __('Order by ("none", "title", "date", "ID").', 'aqua')
            ),
            array(
                "type" => "dropdown",
                "heading" => __('Order', 'aqua'),
                "param_name" => "order",
                "value" => Array(
                    "None" => "none",
                    "ASC" => "ASC",
                    "DESC" => "DESC"
                ),
                "description" => __('Order ("None", "Asc", "Desc").', 'aqua')
            ),
           
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", 'aqua'),
                "param_name" => "el_class",
                "value" => "",
				"description" => __ ( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'aqua' )
            ),
        )
    ));
}
