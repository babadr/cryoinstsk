<?php

vc_map ( array (

		"name" => 'Gallery',

		"base" => "gallery_type",

		"icon" => "tb-icon-for-vc",

		"category" => __( 'Aqua', 'aqua' ), 

		'admin_enqueue_js' => array(URI_PATH_FR .'/admin/assets/js/customvc.js'),

		"params" => array (
				array(
					"type" => "dropdown",
					
					"class" => "",
					
					"heading" => __("Template", 'aqua'),
					
					"param_name" => "tpl",
					
					"value" => array(
					
						__("Template 1 ~ Yoga",'aqua') => "tpl1",
						
						__("Template 2 ~ Fitness",'aqua') => "tpl2",
						
						__("Template 3 ~ Inline",'aqua') => "tpl3"
						
					),
					
					"description" => __('Select template in this element.', 'aqua')
					
				),
				array (

						"type" => "tb_taxonomy",

						"taxonomy" => "gallery_category",

						"heading" => __( "Gallery Categories", 'aqua' ),

						"param_name" => "category",

						"description" => __( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'aqua' )

				),

				array (

						"type" => "textfield",

						"heading" => __( 'Count', 'aqua' ),

						"param_name" => "posts_per_page",

						'value' => '',

						"description" => __( 'The number of posts to display on each page. Set to "-1" for display all posts on the page.', 'aqua' )

				),
				
				array(

					"type" => "checkbox",

					"class" => "",

					"heading" => __("Show Title", 'aqua'),

					"param_name" => "show_title",

					"value" => array (

						__( "Yes, please", 'aqua' ) => true

					),
					"std" => 1,

					"group" => __("Template", 'aqua'),
					"description" => __("Show or not title items in this element.", 'aqua')

				),
				
				array(

					"type" => "checkbox",

					"class" => "",

					"heading" => __("Show excerpt", 'aqua'),

					"param_name" => "show_excerpt",

					"value" => array (

						__( "Yes, please", 'aqua' ) => true

					),
					"std" => 1,
					"group" => __("Template", 'aqua'),
					"description" => __("Show or not excerpt items in this element", 'aqua')
				),
				 array(
					"type" => "checkbox",
					"heading" => __('Show Pagination', 'aqua'),
					"param_name" => "show_pagination",
					 "value" => array(
						__("Yes, please", 'aqua') => 1
					),
					'std' => 0,
					'dependency'=> array(
						'element' => 'tpl',
						'value' => 'tpl1'
					),
					"group" => __("Template", 'aqua'),
					"description" => __('Show or hide pagination of post on your classes.', 'aqua')
				),
				array(
					"type" => "dropdown",
					"heading" => __('Pagination Position', 'aqua'),
					"param_name" => "pos_pagination",
					"value" => Array(
						"Left" => "text-left",
						"Center" => "text-center",
						"Right" => "text-right"
					),
					'dependency'=> array(
						'element' => 'tpl',
						'value' => 'tpl1'
					),
					"group" => __("Template", 'aqua'),
					'std' => "text-center",
					"description" => __('Select Pagination Position.', 'aqua')
				),
				array (

					"type" => "dropdown",

					"heading" => __( 'Order by', 'aqua' ),

					"param_name" => "orderby",

					"value" => array (

							__("None",'aqua') => "none",

							__("Title",'aqua') => "title",

							__("Date",'aqua') => "date",

							__("ID",'aqua') => "ID"

					),

					"description" => __( 'Order by ("none", "title", "date", "ID").', 'aqua' )

				),

				array (

						"type" => "dropdown",

						"heading" => __( 'Order', 'aqua' ),

						"param_name" => "order",

						"value" => Array (

								__("None",'aqua') => "none",

								__("ASC",'aqua') => "ASC",

								__("DESC",'aqua') => "DESC"

						),

						"description" => __( 'Order ("None", "Asc", "Desc").', 'aqua' )

				),

				array(

					"type" => "textfield",

					"class" => "",

					"heading" => __("Extra Class", 'aqua'),

					"param_name" => "el_class",

					"value" => "",

					"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'aqua' )

				),
					
		)

));