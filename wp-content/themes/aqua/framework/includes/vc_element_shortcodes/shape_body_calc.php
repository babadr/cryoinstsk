<?php
vc_map ( array (
	"name" => 'Shape Body Calculator',
	"base" => "shape_calculator",
	"icon" => "tb-icon-for-vc",
	"category" => __ ( 'Aqua', 'aqua' ), 
	'admin_enqueue_js' => array(URI_PATH_FR.'/admin/assets/js/customvc.js'),
	"params" => array (
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Template", 'aqua'),
			"param_name" => "tpl",
			"value" => array(
				__("Template 1",'aqua') => "tpl1",
				__("Template 2",'aqua') => "tpl2",
				__("Template 3",'aqua') => "tpl3",
			),
			"description" => __('Select template in this elment.', 'aqua')
		),
		 array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", 'aqua'),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title.", 'aqua')
            ),
		 array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Sub Title", 'aqua'),
                "param_name" => "subtitle",
                "value" => "",
                "description" => __("Sub Title.", 'aqua')
            ),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Extra Class", 'aqua'),
			"param_name" => "el_class",
			"value" => "",
			"description" => __ ( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'aqua' )
		),
		
	)
));