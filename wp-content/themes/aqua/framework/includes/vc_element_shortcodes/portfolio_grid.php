<?php
vc_map ( array (
		"name" => 'Portfolio Grid',
		"base" => "tb_portfolio_grid",
		"icon" => "tb-icon-for-vc",
		"category" => __ ( 'Aqua', 'aqua' ), 
		'admin_enqueue_js' => array(URI_PATH_FR.'/admin/assets/js/customvc.js'),
		"params" => array (
				array (
						"type" => "tb_taxonomy",
						"taxonomy" => "portfolio_category",
						"heading" => __ ( "Categories", 'aqua' ),
						"param_name" => "category",
						"description" => __ ( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'aqua' )
				),
				array (
						"type" => "dropdown",
						"class" => "",
						"heading" => __ ( "Template", 'aqua' ),
						"param_name" => "tpl",
						"value" => array(
							__("Template 1",'aqua') => "tpl1",
						),
						"description" => __ ( "", 'aqua' )
				),
				array (
						"type" => "textfield",
						"heading" => __ ( 'Count', 'aqua' ),
						"param_name" => "posts_per_page",
						'value' => '12',
						"description" => __ ( 'The number of posts to display on each page. Set to "-1" for display all posts on the page.', 'aqua' )
				),
				array (
						"type" => "dropdown",
						"heading" => __ ( 'Order by', 'aqua' ),
						"param_name" => "orderby",
						"value" => array (
								"None" => "none",
								"Title" => "title",
								"Date" => "date",
								"ID" => "ID"
						),
						"description" => __ ( 'Order by ("none", "title", "date", "ID").', 'aqua' )
				),
				array (
						"type" => "dropdown",
						"heading" => __ ( 'Order', 'aqua' ),
						"param_name" => "order",
						"value" => Array (
								"None" => "none",
								"ASC" => "ASC",
								"DESC" => "DESC"
						),
						"description" => __ ( 'Order ("None", "Asc", "Desc").', 'aqua' )
				),
				array (
						"type" => "textfield",
						"heading" => __ ( "Extra class name", "js_composer" ),
						"param_name" => "el_class",
						"description" => __ ( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer" )
				)
		)
));