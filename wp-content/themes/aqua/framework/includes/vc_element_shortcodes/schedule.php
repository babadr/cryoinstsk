<?php

add_action('init', 'schedule_integrateWithVC');

function schedule_integrateWithVC() {
    vc_map(array(
        "name" => __("Schedule Classles", 'aqua'),
        "base" => "schedule",
        "class" => "schedule",
        "category" => __('Aqua', 'aqua'),
        "icon" => "tb-icon-for-vc",
        "params" => array(
		  array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Opening times", 'aqua'),
                "param_name" => "begin",
                "value" => "",
				"std" => 7,
                "description" => __("Opening time.", 'aqua')
            ),
			  array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Closing time", 'aqua'),
                "param_name" => "end",
                "value" => "",
				"std" => 20,
                "description" => __("Closing time.", 'aqua')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", 'aqua'),
                "param_name" => "el_class",
                "value" => "",
                "description" => __ ( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'aqua' )
            ),
        )
    ));
}
