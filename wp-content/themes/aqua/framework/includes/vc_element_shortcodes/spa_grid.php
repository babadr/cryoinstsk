<?php
add_action('init', 'tb_spa_integrateWithVC');

function tb_spa_integrateWithVC() {
    vc_map(array(
        "name" => __("spa grid", 'aqua'),
        "base" => "tb_spa_grid",
        "class" => "tb-spa-grid",
        "category" => __('Aqua', 'aqua'),
        'admin_enqueue_js' => array(URI_PATH_ADMIN.'/assets/js/customvc.js'),
        "icon" => "tb-icon-for-vc",
        "params" => array(
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Post Count", 'aqua'),
                "param_name" => "posts_per_page",
                "value" => "",
				"description" => __('Please, enter number of post per page for spa. Show all: -1.', 'aqua')
            ),
            array (
                "type" => "tb_taxonomy",
                "taxonomy" => "spa_category",
                "heading" => __ ( "Categories", 'aqua' ),
                "param_name" => "spa_category",
                "class" => "",
                "description" => __ ( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'aqua' )
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns", 'aqua'),
                "param_name" => "columns",
                "value" => array(
                    "4 Columns" => "4",
                    "3 Columns" => "3",
                    "2 Columns" => "2",
                    "1 Column" => "1",
                ),
				"description" => __('Select columns for spa.', 'aqua')
            ),
			array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Deviation Text Align", 'aqua'),
                "param_name" => "deviation_text_align",
                "value" => array(
                    "Left" => "left",
                    "Right" => "right",
                    "Bottom" => "bottom",
                ),
				"description" => __('Select deviation style for spa.', 'aqua')
            ),
            array(
                "type" => "checkbox", 
                "heading" => __('Crop image', 'aqua'),
                "param_name" => "crop_image",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				"group" => __("Template", 'aqua'),
                "description" => __('Crop or not crop image on your Post.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Width image', 'aqua'),
                "param_name" => "width_image",
				"group" => __("Template", 'aqua'),
                "description" => __('Enter the width of image. Default: 300.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Height image', 'aqua'),
                "param_name" => "height_image",
				"group" => __("Template", 'aqua'),
                "description" => __('Enter the height of image. Default: 200.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Title', 'aqua'),
                "param_name" => "show_title",
				"group" => __("Template", 'aqua'),
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
                "description" => __('Show or hide title of post on your spa.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Price', 'aqua'),
                "param_name" => "show_price",
				"group" => __("Template", 'aqua'),
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
                "description" => __('Show or hide price of post on your spa.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Excerpt', 'aqua'),
                "param_name" => "show_desc",
				"group" => __("Template", 'aqua'),
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
                "description" => __('Show or hide excerpt of post on your spa.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Excerpt Length', 'aqua'),
                "param_name" => "excerpt_length",
				"group" => __("Template", 'aqua'),
                "value" => '',
				'std' => 25,
                "description" => __('The length of the excerpt, number of words to display. Set -1 show all words of excerpt.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Excerpt More', 'aqua'),
                "param_name" => "excerpt_more",
				"group" => __("Template", 'aqua'),
                "value" => "",
				"description" => __('Please enter excerpt more for spa.', 'aqua')
            ),
            array(
                "type" => "dropdown",
                "heading" => __('Order by', 'aqua'),
                "param_name" => "orderby",
                "value" => array(
                    "None" => "none",
                    "Title" => "title",
                    "Date" => "date",
                    "ID" => "ID"
                ),
                "description" => __('Order by ("none", "title", "date", "ID").', 'aqua')
            ),
            array(
                "type" => "dropdown",
                "heading" => __('Order', 'aqua'),
                "param_name" => "order",
                "value" => Array(
                    "None" => "none",
                    "ASC" => "ASC",
                    "DESC" => "DESC"
                ),
                "description" => __('Order ("None", "Asc", "Desc").', 'aqua')
            ),
         
			array(
                "type" => "dropdown",
                "heading" => __('Object Animation', 'aqua'),
                "param_name" => "ob_animation",
                "value" => Array(
                    "Wrap" => "wrap",
                    "Item" => "item"
                ),
                "description" => __('Select object animation', 'aqua')
            ),
			array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Animation", 'aqua'),
                "param_name" => "animation",
                "value" => array(
                    "No" => "",
                    "Top to bottom" => "top-to-bottom",
                    "Bottom to top" => "bottom-to-top",
                    "Left to right" => "left-to-right",
                    "Right to left" => "right-to-left",
                    "Appear from center" => "appear"
                ),
                "description" => __("Box Animation", 'aqua')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", 'aqua'),
                "param_name" => "el_class",
                "value" => "",
				"description" => __ ( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'aqua' )
            ),
        )
    ));
}
