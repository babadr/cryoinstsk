<?php

add_action('init', 'pricing_integrateWithVC');

function pricing_integrateWithVC() {
    vc_map(array(
        "name" => __("Pricing plan", 'aqua'),
        "base" => "pricing",
        "class" => "pricing",
        "category" => __('Aqua', 'aqua'),
        "icon" => "tb-icon-for-vc",
        "params" => array(
			array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Template", 'aqua'),
                "param_name" => "tpl",
                "value" => array(
					__("Template 1",'aqua') => "tpl1",
					__("Template 2",'aqua') => "tpl2",
					__("Template 3",'aqua') => "tpl3",
                ),
                "description" => __('Select template in this elment.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", 'aqua'),
                "param_name" => "title",
                "value" => "",
                "description" => __("Content.", 'aqua')
            ),
			array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Sub title", 'aqua'),
                "param_name" => "sub_title",
                "value" => "",
				"dependency" => array(
					"element"=>"tpl",
					"value"=> array("tpl1","tpl2"),
				),
                "description" => __("Input Subtitle.", 'aqua')
            ),
			array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Price", 'aqua'),
                "param_name" => "price",
                "value" => "",
				'dependency' => array(
					'element' => 'tpl',
					"value"=> "tpl2",
				),
                "description" => __("Price.", 'aqua')
            ),
			array(
                "type" => "attach_image",
                "class" => "",
                "heading" => __("Image", 'aqua'),
                "param_name" => "bg_image",
                "value" => "",
                "dependency" => array(
					"element"=>"tpl",
					"value"=> array("tpl3","tpl2"),
				),
                "description" => __("Select image for this element background.", 'aqua')
            ),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => __("Background", 'aqua'),
				"param_name" => "color",
				"value" => "",
				"dependency" => array(
					"element"=>"tpl",
					"value"=> "tpl3",
				),
				"description" => __('Select background color in this element.', 'aqua')
			),
			array(
				"type" => "textarea_html",
				"class" => "",
				"heading" => __("Description", 'aqua'),
				"param_name" => "content",
				"value" => "",
				"description" => __("Please, enter description in this element.", 'aqua')
			), 
			array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Text button", 'aqua'),
                "param_name" => "btn_text",
                "value" => "",
				'std' => __('get started','aqua'),
				"dependency" => array(
					"element"=>"tpl",
					"value"=> array("tpl1","tpl2"),
				),
                "description" => __("Input Subtitle.", 'aqua')
            ),
			array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Link button", 'aqua'),
                "param_name" => "ex_link",
                "value" => "",
				'std'=> '#',
				"dependency" => array(
					"element"=>"tpl",
					"value"=> array("tpl1","tpl2"),
				),
                "description" => __("link button.", 'aqua')
            ),			
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", 'aqua'),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", 'aqua')
            ),
        )
    ));
}
