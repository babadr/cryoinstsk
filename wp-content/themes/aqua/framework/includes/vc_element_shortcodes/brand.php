<?php
vc_map(array(
	"name" => __("Brand", 'aqua'),
	"base" => "brand_logo",
	"category" => __('Aqua', 'aqua'),
	"icon" => "tb-icon-for-vc",
	"params" => array(
		array(
			"type" => "attach_image",
			"class" => "",
			"heading" => __("Logo", 'aqua'),
			"param_name" => "logo",
			"value" => "",
			"description" => __("Select logo in this element.", 'aqua')
		),
		array(
			"type" => "attach_image",
			"class" => "",
			"heading" => __("Logo Active", 'aqua'),
			"param_name" => "logo_active",
			"value" => "",
			"description" => __("Select logo active in this element.", 'aqua')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Logo Url", 'aqua'),
			"param_name" => "logo_url",
			"value" => "",
			"description" => __("Please, enter logo url in this element.", 'aqua')
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Extra Class", 'aqua'),
			"param_name" => "el_class",
			"value" => "",
			"description" => __ ( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'aqua' )
		),
	)
));
