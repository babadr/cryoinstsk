<?php get_header(); ?>
<?php
global $tb_options;
$tb_show_page_title = isset($tb_options['tb_post_show_page_title']) ? $tb_options['tb_post_show_page_title'] : 1;
$tb_show_page_breadcrumb = isset($tb_options['tb_post_show_page_breadcrumb']) ? $tb_options['tb_post_show_page_breadcrumb'] : 1;
tb_theme_title_bar($tb_show_page_title, $tb_show_page_breadcrumb);

$tb_show_post_nav = (int) isset($tb_options['tb_post_show_post_nav']) ?  $tb_options['tb_post_show_post_nav']: 1;
$tb_show_post_comment = (int) isset($tb_options['tb_post_show_post_comment']) ?  $tb_options['tb_post_show_post_comment']: 1;

?>
	<div class="main-content">
		<div class="container">
			<div class="row">
				<?php
				$tb_blog_layout = isset($tb_options['tb_post_layout']) ? $tb_options['tb_post_layout'] : '3cm';
				
				$cl_content = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
				$cl_sb_left = '';
				$cl_sb_right = '';
				
				switch ($tb_blog_layout) {
					case '1col':
						$cl_content = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
						$cl_sb_left = '';
						$cl_sb_right = '';
						break;
					case '2cl':
						if(is_active_sidebar( 'tbtheme-left-sidebar' )){
							$cl_content = 'col-xs-12 col-sm-9 col-md-9 col-lg-9';
							$cl_sb_left = 'col-xs-12 col-sm-3 col-md-3 col-lg-3';
						}
						break;
					case '2cr':
						if(is_active_sidebar( 'tbtheme-right-sidebar' )){
							$cl_content = 'col-xs-12 col-sm-9 col-md-9 col-lg-9';
							$cl_sb_right = 'col-xs-12 col-sm-3 col-md-3 col-lg-3';
						}
						break;
				}
				?>
				<!-- Start Left Sidebar -->
				<?php if($tb_blog_layout == '2cl' && is_active_sidebar( 'tbtheme-left-sidebar' )){ ?>
					<div class="<?php echo esc_attr($cl_sb_left) ?> sidebar-left">
						<?php get_sidebar('left'); ?>
					</div>
				<?php } ?>
				<!-- End Left Sidebar -->
				<!-- Start Content -->
				<div class="<?php echo esc_attr($cl_content) ?> content tb-blog">
					<?php
					while ( have_posts() ) : the_post();
						$post_id = get_the_ID();
						get_template_part( 'framework/templates/classes/single/entry', get_post_format());
						
						// If comments are open or we have at least one comment, load up the comment template.
						if ( (comments_open() && $tb_show_post_comment) || (get_comments_number() && $tb_show_post_comment) ) comments_template();
					endwhile;
					?>
				</div>
				<!-- End Content -->
				<!-- Start Right Sidebar -->
				<?php if($tb_blog_layout == '2cr' && is_active_sidebar( 'tbtheme-classes-sidebar' )){ ?>
					<div class="<?php echo esc_attr($cl_sb_right) ?> sidebar-right">
						<?php if(is_active_sidebar('tbtheme-classes-sidebar')){ dynamic_sidebar("tbtheme-classes-sidebar"); }?>
					</div>
				<?php } ?>
				<!-- End Right Sidebar -->
			</div>

		</div>
	</div>
<?php get_footer(); ?>