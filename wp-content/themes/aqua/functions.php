<?php
	/* Define THEME */
	if (!defined('URI_PATH')) define('URI_PATH', get_template_directory_uri());
	if (!defined('ABS_PATH')) define('ABS_PATH', get_template_directory());
	if (!defined('URI_PATH_FR')) define('URI_PATH_FR', URI_PATH.'/framework');
	if (!defined('ABS_PATH_FR')) define('ABS_PATH_FR', ABS_PATH.'/framework');
	if (!defined('URI_PATH_ADMIN')) define('URI_PATH_ADMIN', URI_PATH_FR.'/admin');
	if (!defined('ABS_PATH_ADMIN')) define('ABS_PATH_ADMIN', ABS_PATH_FR.'/admin');
	/* Theme Options */
	function tb_filtercontent($variable){
		return $variable;
	}
	if ( !class_exists( 'ReduxFramework' ) ) {
		require_once( ABS_PATH . '/redux-framework/ReduxCore/framework.php' );
	}
	require_once (ABS_PATH_ADMIN.'/theme-options.php');
	require_once (ABS_PATH_ADMIN.'/index.php');
	global $tb_options;
	/* Template Functions */
	require_once ABS_PATH_FR . '/template-functions.php';
	/* Template Functions */
	require_once ABS_PATH_FR . '/templates/post-favorite.php';
	require_once ABS_PATH_FR . '/templates/post-functions.php';
	/* Lib resize images */
	require_once ABS_PATH_FR.'/includes/resize.php';
	require_once ABS_PATH_FR.'/includes/custom-post-templates.php';
	/* Post Type */
	require_once ABS_PATH_FR.'/post-type/trainer.php';
	require_once ABS_PATH_FR.'/post-type/classes.php';
	require_once ABS_PATH_FR.'/post-type/portfolio.php';
	require_once ABS_PATH_FR.'/post-type/space.php';
	require_once ABS_PATH_FR.'/post-type/testimonial.php';
	require_once ABS_PATH_FR.'/post-type/team.php';
	require_once ABS_PATH_FR.'/post-type/client.php';
	require_once ABS_PATH_FR.'/post-type/gallery.php';
	require_once ABS_PATH_FR.'/post-type/events.php';
	require_once ABS_PATH_FR.'/post-type/spa.php';
	/* Function for OCDI */
	function _aqua_filter_fw_ext_backups_demos($demos)
	{
		$demos_array = array(
			'aqua' => array(
				'title' => esc_html__('AQUA DEMO', 'aqua'),
				'screenshot' => 'http://jwsuperthemes.com/import_demo/aqua/screenshot.png',
				'preview_link' => 'http://aqua.jwsuperthemes.com',
			),
		);
        $download_url = 'http://jwsuperthemes.com/import_demo/aqua/download-script/';
		foreach ($demos_array as $id => $data) {
			$demo = new FW_Ext_Backups_Demo($id, 'piecemeal', array(
				'url' => $download_url,
				'file_id' => $id,
			));
			$demo->set_title($data['title']);
			$demo->set_screenshot($data['screenshot']);
			$demo->set_preview_link($data['preview_link']);

			$demos[$demo->get_id()] = $demo;

			unset($demo);
		}

		return $demos;
	}
	add_filter('fw:ext:backups-demo:demos', '_aqua_filter_fw_ext_backups_demos');	
	/* Function for Framework */
	require_once ABS_PATH_FR . '/includes.php';
	/* Register Sidebar */
	if (!function_exists('tb_RegisterSidebar')) {
		function tb_RegisterSidebar(){
			global $tb_options;
			register_sidebar(array(
			'name' => __('Right Sidebar', 'aqua'),
			'id' => 'tbtheme-right-sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Left Sidebar', 'aqua'),
			'id' => 'tbtheme-left-sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Classes Sidebar', 'aqua'),
			'id' => 'tbtheme-classes-sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Shop Update Sidebar ', 'aqua'),
			'id' => 'tbtheme-shop-update-sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Single Basic', 'aqua'),
			'id' => 'tbtheme-single-basic',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
		register_sidebar(array(
			'name' => __('Footer Newsletter', 'aqua'),
			'id' => 'tbtheme-footer-newsletter',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
		register_sidebars(2,array(
		'name' => __('Header 4 Top Widget %d', 'aqua'),
		'id' => 'tbtheme-header-4-top-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<span class="wg-title mm">',
		'after_title' => '</span>',
		));
		register_sidebars(3,array(
		'name' => __('Header 5 Top Widget %d', 'aqua'),
		'id' => 'tbtheme-header-5-top-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<h3 class="wg-title">',
		'after_title' => '</h3>',
		));
		register_sidebars(2,array(
		'name' => __('Header 6 Top Widget %d', 'aqua'),
		'id' => 'tbtheme-header-6-top-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<h3 class="wg-title">',
		'after_title' => '</h3>',
		));
		register_sidebars(2,array(
		'name' => __('Header 8 Top Widget %d', 'aqua'),
		'id' => 'tbtheme-header-6-top-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<h3 class="wg-title">',
		'after_title' => '</h3>',
		));
		register_sidebars(4,array(
		'name' => __('Footer Top Widget %d', 'aqua'),
		'id' => 'tbtheme-footer-top-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<span class="wg-title myfooter">',
		'after_title' => '</span>',
		));
		register_sidebars(2,array(
		'name' => __('Footer Bottom Widget %d', 'aqua'),
		'id' => 'tbtheme-footer-bottom-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<span class="wg-title fot">',
		'after_title' => '</span>',
		));
		register_sidebars(4,array(
		'name' => __('Footer 2 Top Widget %d', 'aqua'),
		'id' => 'tbtheme-footer2-top-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<span class="wg-title">',
		'after_title' => '</span>',
		));
		register_sidebars(2,array(
		'name' => __('Footer 2 Bottom Widget %d', 'aqua'),
		'id' => 'tbtheme-footer2-bottom-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<span class="wg-title">',
		'after_title' => '</span>',
		));
		register_sidebars(4,array(
			'name' => __('Footer 3 Top Widget %d', 'aqua'),
			'id' => 'tbtheme-footer3-top-widget',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div style="clear:both;"></div></div>',
			'before_title' => '<span class="wg-title">',
			'after_title' => '</span>',
		));
		register_sidebars(2,array(
			'name' => __('Footer 3 Bottom Widget %d', 'aqua'),
			'id' => 'tbtheme-footer3-bottom-widget',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div style="clear:both;"></div></div>',
			'before_title' => '<span class="wg-title">',
			'after_title' => '</span>',
		));
		
		register_sidebars(4,array(
			'name' => __('Footer 4 Top Widget %d', 'aqua'),
			'id' => 'tbtheme-footer4-top-widget',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div style="clear:both;"></div></div>',
			'before_title' => '<span class="wg-title ll">',
			'after_title' => '</span>',
		));
		register_sidebars(2,array(
			'name' => __('Footer 4 Bottom Widget %d', 'aqua'),
			'id' => 'tbtheme-footer4-bottom-widget',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div style="clear:both;"></div></div>',
			'before_title' => '<span class="wg-title">',
			'after_title' => '</span>',
		));
		
		
		register_sidebars(5 ,array(
			'name' => __('Footer 5 Top Widget %d', 'aqua'),
			'id' => 'tbtheme-footer5-top-widget',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div style="clear:both;"></div></div>',
			'before_title' => '<span class="wg-title">',
			'after_title' => '</span>',
		));
		register_sidebars(1 ,array(
			'name' => __('Footer 5 Bottom Widget 1', 'aqua'),
			'id' => 'tbtheme-footer5-bottom-widget',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div style="clear:both;"></div></div>',
			'before_title' => '<span class="wg-title">',
			'after_title' => '</span>',
		));
		if (class_exists ( 'Woocommerce' )) {
		register_sidebar(array(
		'name' => __('Woocommerce Sidebar', 'aqua'),
		'id' => 'tbtheme-woo-sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="wg-title"><span>',
		'after_title' => '</span></h3>',
		));
		register_sidebar(array(
		'name' => __('Woocommerce Single Sidebar', 'aqua'),
		'id' => 'tbtheme-woo-single-sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="wg-title">',
		'after_title' => '</h3>',
		));
		register_sidebars(2,array(
		'name' => __('Header Shop Top Widget %d', 'aqua'),
		'id' => 'tbtheme-header-shop-top-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<h3 class="wg-title">',
		'after_title' => '</h3>',
		));
		register_sidebars(2,array(
		'name' => __('Header Shop Widget %d', 'aqua'),
		'id' => 'tbtheme-header-shop-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<h3 class="wg-title">',
		'after_title' => '</h3>',
		));
		}
		register_sidebars(4,array(
		'name' => __('Custom Widget %d', 'aqua'),
		'id' => 'tbtheme-custom-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<h3 class="wg-title">',
		'after_title' => '</h3>',
		));		
		}
		}
		add_action( 'init', 'tb_RegisterSidebar' );
		/* Add Stylesheet And Script */
		function tb_theme_enqueue_style() {
		global $tb_options;
		if ($tb_options["tb_responsive"]) {
		wp_enqueue_style( 'bootstrap.min', URI_PATH.'/assets/css/bootstrap.min.css', false );
		}else{
		wp_enqueue_style( 'bootstrap-no-responsive', URI_PATH.'/assets/css/bootstrap-no-responsive.css', false );
		}
		//wp_enqueue_style('owl-carousel', URI_PATH . "/assets/vendors/owl-carousel/owl.carousel.css",array(),"");
		wp_enqueue_style('flexslider.css', URI_PATH . "/assets/vendors/flexslider/flexslider.css",array(),"");
		wp_enqueue_style('jquery.mCustomScrollbar', URI_PATH . "/assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css",array(),"");
		wp_enqueue_style('jquery.fancybox', URI_PATH . "/assets/vendors/FancyBox/jquery.fancybox.css",array(),"");
		wp_enqueue_style('colorbox', URI_PATH . "/assets/css/colorbox.css",array(),"");
		wp_enqueue_style('font-awesome', URI_PATH.'/assets/css/font-awesome.min.css', array(), '4.1.0');
		wp_enqueue_style('font-ionicons', URI_PATH.'/assets/css/ionicons.min.css', array(), '1.5.2');
		wp_enqueue_style('font-aqua', URI_PATH.'/assets/css/font-aqua.css', array(), '');
		wp_enqueue_style('uikit.min', URI_PATH.'/assets/css/uikit.min.css', array(), '2.8.0');
		wp_enqueue_style( 'tb.core.min', URI_PATH.'/assets/css/tb.core.min.css', false );
		if(class_exists('WooCommerce')){
		wp_enqueue_style( 'woocommerce', URI_PATH . '/assets/css/woocommerce.css', array(), '1.0.0');
		}
		wp_enqueue_style( 'shortcodes', URI_PATH_FR.'/shortcodes/shortcodes.css', false );
		wp_enqueue_style( 'main-style', URI_PATH.'/assets/css/main-style.css', false );
		wp_enqueue_style( 'style', URI_PATH.'/style.css', false );	
		}
		add_action( 'wp_enqueue_scripts', 'tb_theme_enqueue_style' );
		function tb_theme_enqueue_script() {
		global $tb_options;
		$tb_smoothscroll = $tb_options["tb_smoothscroll"];
		wp_enqueue_script("jquery");
		wp_enqueue_script( 'datepicker.min', URI_PATH.'/assets/js/datepicker.min.js', array('jquery'), '', true  );
		wp_enqueue_script( 'jquery.flexslider-min', URI_PATH.'/assets/vendors/flexslider/jquery.flexslider-min.js', array('jquery') );
		wp_register_script( 'countUP', URI_PATH.'/assets/js/jquery.incremental-counter.min.js', array('jquery'), false, true );
		wp_enqueue_script( 'jquery.mCustomScrollbar', URI_PATH.'/assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js', array('jquery') );
		wp_enqueue_script( 'jquery.fancybox', URI_PATH.'/assets/vendors/FancyBox/jquery.fancybox.js', array('jquery') );
		wp_enqueue_script( 'jquery.elevatezoom', URI_PATH.'/assets/vendors/elevatezoom-master/jquery.elevatezoom.js', array('jquery') );
		wp_enqueue_script( 'dotdotdot.min', URI_PATH.'/assets/js/jquery.dotdotdot.min.js', array('jquery') );
		wp_enqueue_script('modernizr', URI_PATH . '/assets/js/modernizr.custom.26633.js',array(),"1");
		wp_enqueue_script('jquery.gridrotator', URI_PATH . '/assets/js/jquery.gridrotator.js',array(),"1");
		wp_enqueue_script( 'bootstrap.min', URI_PATH.'/assets/js/bootstrap.min.js', array('jquery') );
		wp_enqueue_script('jquery.colorbox', URI_PATH . "/assets/js/jquery.colorbox.js", array('jquery'),"1.5.5");
		wp_enqueue_script( 'tb.shortcodes', URI_PATH_FR.'/shortcodes/shortcodes.js', array('jquery') );
		wp_enqueue_script( 'parallax', URI_PATH.'/assets/js/parallax.js', array('jquery') );
		wp_enqueue_script( 'uikit.min', URI_PATH.'/assets/js/uikit.min.js', array('jquery') );
		//wp_enqueue_script( 'owl.carousel.min', URI_PATH.'/assets/vendors/owl-carousel/owl.carousel.min.js', array('jquery') );
		wp_enqueue_script( 'main', URI_PATH.'/assets/js/main.js', array('jquery') );
		if($tb_smoothscroll){
		wp_enqueue_script( 'SmoothScroll', URI_PATH.'/assets/js/SmoothScroll.js', array('jquery'), '', true );
		}
		wp_localize_script(
			'main',
			'the_ajax_script',
				array(
					'ajaxurl' => admin_url( 'admin-ajax.php')
				)
			);
		}
		add_action( 'wp_enqueue_scripts', 'tb_theme_enqueue_script' );
		/*Style Inline*/
		require ABS_PATH_FR.'/style-inline.php';
		/* Header */
		function tb_Header() {
		global $tb_options,$post;
		$header_layout = $tb_options["tb_header_layout"];
		if($post){
		$tb_header = get_post_meta($post->ID, 'tb_header', true)?get_post_meta($post->ID, 'tb_header', true):'global';
		$header_layout = $tb_header=='global'?$header_layout:$tb_header;
		}
		switch ($header_layout) {
		case 'v1':
		get_template_part('framework/headers/header', 'v1');
		break;
		case 'v2':
		get_template_part('framework/headers/header', 'v2');
		break;
		case 'v3':
		get_template_part('framework/headers/header', 'v3');
		break;
		case 'v4':
		get_template_part('framework/headers/header', 'v4');
		break;
		case 'v5':
		get_template_part('framework/headers/header', 'v5');
		break;
		case 'v6':
		get_template_part('framework/headers/header', 'v6');
		break;
		case 'v7':
		get_template_part('framework/headers/header', 'v7');
		break;
		case 'v8':
		get_template_part('framework/headers/header', 'v8');
		break;
		case 'shop':
		get_template_part('framework/headers/header', 'shop');
		break;
		default :
		get_template_part('framework/headers/header', 'v1');
		break;
		}
		}
		/* Less */
		if(isset($tb_options['tb_less'])&&$tb_options['tb_less']){
		require_once ABS_PATH_FR.'/presets.php';
		}
		/* Widgets */
		require_once ABS_PATH_FR.'/widgets/abstract-widget.php';
		require_once ABS_PATH_FR.'/widgets/widgets.php';
		/* Woo commerce function */
		if (class_exists('Woocommerce')) {
		require_once ABS_PATH . '/woocommerce/wc-template-function.php';
		require_once ABS_PATH . '/woocommerce/wc-template-hooks.php';
		}
		// Hooks add play btn for editor
		add_action('admin_init', 'tb_add_button_play');
		function tb_add_button_play() {
		add_filter('mce_external_plugins', 'tb_add_plugin_play');
		add_filter('mce_buttons', 'tb_register_button_play');
		}
		if (!function_exists('_aqua_deactivate_plugins')){
        function _aqua_deactivate_plugins() {
            deactivate_plugins(array(
                'brizy/brizy.php'
            ));        
            }
        }
        add_action( 'admin_init', '_aqua_deactivate_plugins' );
		function tb_register_button_play($buttons) {
		array_push($buttons, "btnplay");
		return $buttons;
		}
		function tb_add_plugin_play($plugin_array) {
		$plugin_array['btnplay'] = URI_PATH .'/assets/js/mce.btn.play.js';
		return $plugin_array;
		}		
		//* TN - Remove Query String from Static Resources
function remove_css_js_ver( $src ) {
if( strpos( $src, '?ver=' ) )
$src = remove_query_arg( 'ver', $src );
return $src;
}
add_filter( 'style_loader_src', 'remove_css_js_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_css_js_ver', 10, 2 );



function defer_parsing_of_js ( $url ) {
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    if ( strpos( $url, 'jquery.js' ) ) return $url;
    return "$url' defer ";
}
add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
