H�\<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7862;s:11:"post_author";s:1:"3";s:9:"post_date";s:19:"2018-12-12 16:11:29";s:13:"post_date_gmt";s:19:"2018-12-12 15:11:29";s:12:"post_content";s:3899:"[vc_row full_height="yes" parallax="content-moving" content_full_width="1" enable_parallax="1" css=".vc_custom_1544780080487{background-image: url(https://cryoinstitut.fr/wp-content/uploads/2018/12/banner_la-cryolipolyse-definition.jpg?id=8204) !important;background-position: center;background-repeat: no-repeat !important;background-size: cover !important;}" el_class="ro-service-spa"][vc_column][vc_empty_space height="153"][vc_custom_heading text="" font_container="tag:h1|text_align:center|color:%23002c7a"][vc_empty_space height="140"][/vc_column][/vc_row][vc_row][vc_column width="2/3" css=".vc_custom_1545055662478{padding-right: 3% !important;padding-left: 2% !important;}"][vc_empty_space][vc_column_text]
<h3>Thermocryolipolyse,
Les Indications Et Contres Indications:</h3>
<h3>Les indications pour la cryolipolyse</h3>
Le <strong>centre de Cryolipolyse</strong>  à Lyon permet de réaliser une véritable alternative à la chirurgie <strong>(lipoaspiration)</strong> dans le traitement de la <strong>graisse superficielle</strong>.

La <strong>cryolipolyse, ou traitement par le froid</strong> de la cellulite, est un traitement ciblé des amas graisseux localisés : abdomen, flancs « poignées d’amour », « culotte de cheval », faces internes des cuisses et des genoux, bras…
<h3>Adiposités localisées :</h3>
En règle générale les meilleurs répondeurs <strong>(homme ou femme)</strong> à ce traitement sont des patients dont le poids est normal ou dont la <strong>surcharge pondérale n’est pas supérieure à 10%</strong> par rapport à la normale
<h3>Les contre indications pour la cryolipolyse</h3>
Le <strong>traitement d’amincissement</strong> par le procédé de <strong>cryolipolyse</strong> n’est pas indiqué dans un cas d’excédent pondéral trop important ou dans le cas d’une obésité avérée.

Pour les personnes dont la <strong>peau est trop relâchée</strong>, le traitement peut s’avérer déconseillé.

Le <strong>traitement n’est pas envisageable à l’inverse</strong> pour les personnes dont les bourrelets sont trop fins et les tout petit amas graisseux qui manquent de densité.

Enfin, la<strong> thermo cryolipolyse</strong> est proscrit pour les personnes présentant les caractéristiques suivants :

Les <strong>individus atteints d’une forte réactivité au froid du type cryoglobulinémie</strong> (présence de cryogloburines dans le sang), urticaire au froid ou hémoglobinurie paroxystique au froid (anémie hémolytique auto-immune rare).

Mauvaise <strong>circulation sanguine de la zone</strong> traitée et personnes atteintes de la maladie de Raynaud (trouble de la circulation du sang affectant les extrémités comme les <strong>doigts, les orteils, le nez, les oreilles</strong>, etc.).

Hernies
<strong>Grossesse et allaitement</strong>
Patients porteurs de dispositifs électroniques
<strong>Blessure, eczéma, inflammation, chirurgie récente sur la zone traitée</strong>[/vc_column_text][/vc_column][vc_column width="1/3"][vc_empty_space][vc_single_image image="8237" img_size="full" style="vc_box_border"][vc_single_image image="8459" img_size="full" style="vc_box_border"][/vc_column][/vc_row][vc_row content_full_width="1"][vc_column][vc_gmaps link="#E-8_JTNDaWZyYW1lJTIwc3JjJTNEJTIyaHR0cHMlM0ElMkYlMkZ3d3cuZ29vZ2xlLmNvbSUyRm1hcHMlMkZlbWJlZCUzRnBiJTNEJTIxMW0xNCUyMTFtOCUyMTFtMyUyMTFkNTU2Ni4yOTQ5MTQ1ODU1MzUlMjEyZDQuODgzNDclMjEzZDQ1Ljc2ODIzNSUyMTNtMiUyMTFpMTAyNCUyMTJpNzY4JTIxNGYxMy4xJTIxM20zJTIxMW0yJTIxMXMweDAlMjUzQTB4NzA5NGNlZDZiNjQzZWI4OCUyMTJzVmlsbGUlMkJkZSUyQlZpbGxldXJiYW5uZSUyMTVlMCUyMTNtMiUyMTFzZnIlMjEyc2luJTIxNHYxNTQ0NjMxMTM0NDQ1JTIyJTIwd2lkdGglM0QlMjIxMDAlMjUlMjIlMjBoZWlnaHQlM0QlMjI0NTAlMjIlMjBmcmFtZWJvcmRlciUzRCUyMjAlMjIlMjBzdHlsZSUzRCUyMmJvcmRlciUzQTAlMjIlMjBhbGxvd2Z1bGxzY3JlZW4lM0UlM0MlMkZpZnJhbWUlM0U="][/vc_column][/vc_row]";s:10:"post_title";s:33:"indications et contre indications";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:33:"indications-et-contre-indications";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-03-25 09:55:20";s:17:"post_modified_gmt";s:19:"2019-03-25 08:55:20";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:37:"https://cryoinstitut.fr/?page_id=7862";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}