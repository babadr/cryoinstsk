H�\<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7826;s:11:"post_author";s:1:"3";s:9:"post_date";s:19:"2018-12-12 15:53:43";s:13:"post_date_gmt";s:19:"2018-12-12 14:53:43";s:12:"post_content";s:6741:"[vc_row full_height="yes" parallax="content-moving" content_full_width="1" enable_parallax="1" css=".vc_custom_1544781473768{background-image: url(https://cryoinstitut.fr/wp-content/uploads/2018/12/RADIOFREQUENCE-limoges-1.png?id=8262) !important;background-position: center;background-repeat: no-repeat !important;background-size: cover !important;}" el_class="ro-service-spa"][vc_column][vc_empty_space height="153"][vc_custom_heading text="Radiofréquence Visage Bipolaire " font_container="tag:h1|text_align:center|color:%23002c7a" google_fonts="font_family:Roboto%20Slab%3A100%2C300%2Cregular%2C700|font_style:400%20regular%3A400%3Anormal"][vc_empty_space height="140"][vc_column_text]
<p style="text-align: center;"><button style="padding: 5px 10px; color: white; background: #359b93; border: none; border-radius: 50px; font-size: 24px; cursor: pointer;">Je demande mon bilan gratuit </button></p>
[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_empty_space][vc_column_text]
<h2 style="text-align: center;">RADIO FRÉQUENCE VISAGE ET CORPS À LYON (69), CRYO INSTITUT</h2>
[/vc_column_text][/vc_column][/vc_row][vc_row css=".vc_custom_1544781144541{margin-top: 20px !important;padding-top: 10px !important;padding-right: 10px !important;padding-bottom: 10px !important;padding-left: 10px !important;}"][vc_column][vc_video link="https://youtu.be/MMtJoRmY36s" css=".vc_custom_1544796146361{padding-top: 20px !important;padding-right: 20px !important;padding-bottom: 20px !important;padding-left: 20px !important;}"][/vc_column][/vc_row][vc_row][vc_column width="1/2"][vc_column_text]
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<h3 style="text-align: center;"><span style="color: #359b93;">RADIOFRÉQUENCE</span></h3>
</div>
</div>
<div class="vc_empty_space" style="text-align: left;"></div>
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<p style="text-align: left;">Par l’onde diathermique il est possible de restaurer un système adéquat de vascularisation de toutes les zones traitées et de stimuler certains processus naturels, tels que la production de collagène, d’élastine et d’acide hyaluronique, en augmentant leur action.</p>
<p style="text-align: center;">   <a href="https://cryoinstitut.fr/wp-content/uploads/2018/12/Cryoinstitut-RF1.png"><img class="alignnone wp-image-8866 size-full" src="https://cryoinstitut.fr/wp-content/uploads/2018/12/Cryoinstitut-RF1.png" alt="" width="537" height="232" /></a></p>
<p style="text-align: center;"><a href="https://cryoinstitut.fr/wp-content/uploads/2018/12/Cryoinstitut-RF2.png"><img class="alignnone wp-image-8867 size-full" src="https://cryoinstitut.fr/wp-content/uploads/2018/12/Cryoinstitut-RF2.png" alt="" width="537" height="230" /></a></p>

</div>
</div>
[/vc_column_text][/vc_column][vc_column width="1/2"][vc_column_text]
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<h3 style="text-align: center;"><span style="color: #359b93;">LIFTING</span></h3>
</div>
</div>
<div class="vc_empty_space" style="text-align: left;"></div>
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<p style="text-align: left;">Ces courants spécifiques essaient de stimuler les influx nerveux physi­ologiques dans le but de restaurer les tissus. Le lifting électrique est à même de produire une stimulation sur la micro circulation cutanée, qui à son tour, favorise la nutrition et l’oxygénation des tissus tout en favorisant la revitali­sation. Il stimule également les ” fibroblastes » et augmente la quantité et la qualité du collagène synthétisé par ces derniers et favorise le système lymphatique et ses fonctions. Le traitement est divisé en quatre phases et chacune utilise un type de courant différent.</p>
<p style="text-align: center;"><a href="https://cryoinstitut.fr/wp-content/uploads/2018/12/Cryoinstitut-RF3.png"><img class="alignnone wp-image-8869 size-full" src="https://cryoinstitut.fr/wp-content/uploads/2018/12/Cryoinstitut-RF3.png" alt="" width="537" height="231" /></a></p>

</div>
</div>
[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<h3 style="text-align: center;"><span style="color: #359b93;">RADIOFRÉQUENCE BIPOLAIRE</span></h3>
</div>
</div>
<div class="vc_empty_space"></div>
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<p style="text-align: center;">En luttant contre au relâchement de la peau dû au temps, le traitement entraîne la contraction de la peau du visage, avec un effet liftant (rajeu­nissement du visage) visible surtout après quelques mois. En augmentant la température du tissu conjonctif existant et, en incitant par la chaleur les fibroblastes à produire du nouveau collagène et une nouvelle élastine, il permet de réduire les rides et les signes de vieillissement, en offrant à la peau un effet lifting tout à fait naturel.</p>

</div>
<p style="text-align: center;"><a href="https://cryoinstitut.fr/wp-content/uploads/2018/12/Cryoinstitut-RF1.png"><img class="alignnone wp-image-8866 size-full" src="https://cryoinstitut.fr/wp-content/uploads/2018/12/Cryoinstitut-RF1.png" alt="" width="537" height="232" /></a></p>

</div>
[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<h3 style="text-align: center;"><span style="color: #359b93;">RADIOFRÉQUENCE MULTIPOLAIRE</span></h3>
</div>
</div>
<div class="vc_empty_space"></div>
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<p style="text-align: center;">Elle est utilisée pour obtenir le rajeunissement de la peau, grâce à l’au­gmentation de la température sous-cutanée, qui provoque une dénatura­tion des protéines du collagène, qui se rétrécissent et s’épaississent (effet lifting), ce qui conduit à une augmentation de la consistance du derme. La lésion thermique provoquée par la radiofréquence active également les fibroblastes avec une augmentation conséquente de la durée du collagène, de l’élastine, ainsi qu’une augmentation de la densité dermique, de qui pro­voque un plus grand étirement des tissus.</p>

</div>
<p style="text-align: center;"><a href="https://cryoinstitut.fr/wp-content/uploads/2018/12/Cryoinstitut-RF2.png"><img class="alignnone wp-image-8867 size-full" src="https://cryoinstitut.fr/wp-content/uploads/2018/12/Cryoinstitut-RF2.png" alt="" width="537" height="230" /></a></p>

</div>
[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:23:"radio fréquence visage";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:22:"radio-frequence-visage";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-03-25 10:11:07";s:17:"post_modified_gmt";s:19:"2019-03-25 09:11:07";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:37:"https://cryoinstitut.fr/?page_id=7826";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}