H�\<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:9297;s:11:"post_author";s:1:"3";s:9:"post_date";s:19:"2019-01-28 11:13:39";s:13:"post_date_gmt";s:19:"2019-01-28 10:13:39";s:12:"post_content";s:4733:"[vc_row][vc_column][vc_empty_space height="200px"][vc_column_text]
<h1 style="text-align: center;"><strong>Politique de confidentialité</strong></h1>
[/vc_column_text][vc_column_text]<strong>Introduction</strong>
Devant le développement des nouveaux outils de communication, il est nécessaire de porter une attention particulière à la protection de la vie privée. C'est pourquoi, nous nous engageons à respecter la confidentialité des renseignements personnels que nous collectons.

<strong>Collecte des renseignements personnels</strong>

Nous collectons les renseignements suivants :
<ul>
 	<li>Nom,</li>
 	<li>Prénom,</li>
 	<li>Adresse postale,</li>
 	<li>Code postal,</li>
 	<li>Adresse électronique,</li>
 	<li>Numéro de téléphone / télécopieur,</li>
</ul>
Les renseignements personnels que nous collectons sont recueillis au travers de formulaires et grâce à l'interactivité établie entre vous et notre site Web. Nous utilisons également, comme indiqué dans la section suivante, des fichiers témoins et/ou journaux pour réunir des informations vous concernant.

<strong>Formulaires et interactivité:</strong>

Vos renseignements personnels sont collectés par le biais de formulaire, à savoir :
<ul>
 	<li>Formulaire d'inscription au site Web</li>
 	<li>Concours</li>
</ul>
Nous utilisons les renseignements ainsi collectés pour les finalités suivantes :
<ul>
 	<li>Informations / Offres promotionnelles</li>
 	<li>Contact</li>
 	<li>Gestion du site Web (présentation, organisation)</li>
</ul>
Vos renseignements sont également collectés par le biais de l'interactivité pouvant s'établir entre vous et notre site Web et ce, de la façon suivante:
<ul>
 	<li>Contact</li>
 	<li>Gestion du site Web (présentation, organisation)</li>
</ul>
Nous utilisons les renseignements ainsi collectés pour les finalités suivantes :
<ul>
 	<li>Commentaires</li>
 	<li>Informations ou pour des offres promotionnelles</li>
</ul>
<strong>Droit d'opposition et de retrait</strong>

Nous nous engageons à vous offrir un droit d'opposition et de retrait quant à vos renseignements personnels.
Le droit d'opposition s'entend comme étant la possibilité offerte aux internautes de refuser que leurs renseignements personnels soient utilisés à certaines fins mentionnées lors de la collecte.

Le droit de retrait s'entend comme étant la possibilité offerte aux internautes dedemanderà ce que leurs renseignements personnels ne figurent plus, par exemple, dans une liste de diffusion.

Pour pouvoir exercer ces droits, vous pouvez :
Code postal : 8 rue Paul Lafargue, Villeurbanne 69100
Courriel : contact@cryoinstitut.fr
Téléphone : 04 37 23 97 78
Section du site web : https://cryoinstitut.fr/contact/

<strong>Droit d'accès</strong>

Nous nous engageons à reconnaître un droit d'accès et de rectification aux personnes concernées désireuses de consulter, modifier, voire radier les informations les concernant.
L'exercice de ce droit se fera :
<strong>Code postal</strong> : 8 rue Paul Lafargue, Villeurbanne 69100
<strong>Courriel</strong> : contact@cryoinstitut.fr
Téléphone : 04 37 23 97 78
Section du site web : https://cryoinstitut.fr/contact/

<strong>Sécurité</strong>

Les renseignements personnels que nous collectons sont conservés dans un environnement sécurisé. Les personnes travaillant pour nous sont tenues de respecter la confidentialité de vos informations.
Pour assurer la sécurité de vos renseignements personnels, nous avons recours aux mesures suivantes :
<ul>
 	<li>Protocole SSL (Secure Sockets Layer)</li>
 	<li>Gestion des accès - personne concernée</li>
 	<li>Logiciel de surveillance du réseau</li>
 	<li>Sauvegarde informatique</li>
 	<li>Développement de certificat numérique</li>
 	<li>Identifiant / mot de passe</li>
 	<li>Pare-feu (Firewalls)</li>
</ul>
Nous nous engageons à maintenir un haut degré de confidentialité en intégrant les dernières innovations technologiques permettant d'assurer la confidentialité de vos transactions. Toutefois, comme aucun mécanisme n'offre une sécurité maximale, une part de risque est toujours présente lorsque l'on utilise Internet pour transmettre des renseignements personnels.

<strong>Label</strong>

Nos engagements en matière de protection des renseignements personnels répondent aux exigences du programmesuivant :
<ul>
 	<li></li>
 	<li></li>
</ul>
<strong>Législation</strong>

Nous nous engageons à respecter les dispositions législatives énoncées dans :
La GDPR – Règlement Général de la Protection des Données à caractère Personnel, dans l’union.[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:29:"Politique de confidentialité";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:28:"politique-de-confidentialite";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-01-28 12:33:16";s:17:"post_modified_gmt";s:19:"2019-01-28 11:33:16";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:37:"https://cryoinstitut.fr/?page_id=9297";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}