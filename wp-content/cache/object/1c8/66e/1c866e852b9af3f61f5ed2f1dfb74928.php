H�\<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:9359;s:11:"post_author";s:1:"3";s:9:"post_date";s:19:"2019-02-10 12:52:54";s:13:"post_date_gmt";s:19:"2019-02-10 11:52:54";s:12:"post_content";s:5616:"[vc_row][vc_column][vc_empty_space height="200px"][vc_column_text]
<h1 style="text-align: center;">Règlement Intérieur</h1>
<div class="container">
<div class="container_inner default_template_holder clearfix page_container_inner">
<div class="vc_row wpb_row section vc_row-fluid ">
<div class=" full_section_inner clearfix">
<div class="wpb_column vc_column_container vc_col-sm-12">
<div class="vc_column-inner ">
<div class="wpb_wrapper">
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
[/vc_column_text][vc_column_text]Cryo Institut se réserve la possibilité de modifier les présentes conditions générales citées ci-dessous.
Dans ce cas, les conditions générales applicables seront celles en vigueur au moment de la consultation.

&nbsp;
<ol>
 	<li><strong>Réservation de soins esthétiques</strong></li>
</ol>
Les modelages et massages de bien-être contribuent à l’harmonie et à la relaxation du corps.
Ils ne peuvent en aucun cas remplacer des massages médicaux ou un suivi médical en cas de problème de santé
conformément à la loi du 30 avril 1946 au décret 60665 du 4 juillet 1960 à l’article L489 du code de santé
publique et au décret n° 96-879 du 8 octobre 1996, ces massages ne relèvent donc pas du domaine médical ou
de la kinésithérapie mais de techniques de bien-être non sexuelles.

&nbsp;
<ol start="2">
 	<li><strong>Enceinte ? Un traitement médical en cours ?</strong></li>
</ol>
Si vous êtes enceinte, pensez à demander un certificat médical à votre médecin obstétricien pour votre rendezvous
que vous pourrez prendre à partir de votre troisième mois de grossesse.
Si vous souffrez de problèmes de santé (troubles circulatoires, cardiaques, respiratoires, allergies, etc.), nous vous
remercions de bien vouloir en informer notre personnel, certains de nos soins pourraient être contre-indiqués.

&nbsp;
<ol start="3">
 	<li><strong>Tarifs</strong></li>
</ol>
En dehors des offres promotionnelles particulières, les tarifs applicables sont ceux figurant dans le dépliant
tarifaire et sur le site internet www.cryoinstitut.fr en vigueur à la date d’achat.
Pour tout autre achat, les tarifs sont ceux applicables à la date des soins.
Tout achat quel que soit son origine, est payable en euros uniquement.
L’institut Cryo Institut se réserve le droit de modifier les prix de ses prestations.

&nbsp;
<ol start="4">
 	<li><strong>Non présentation au rendez-vous</strong></li>
</ol>
Suite à une réservation de soins esthétiques, l’institut Cryo Institut vous demande de bien vouloir respecter les
horaires retenus.
Pour les esthéticiennes, le temps de service commence à l’heure réservée.
Tout retard correspond à un temps d’attente qui sera compté dans son temps de service ce qui lui donne le droit
d’écourter le temps consacré au soin réservé proportionnellement au retard constaté, sans avertissement
préalable et en cas de non présentation au rendez-vous, passé un délai d’une heure, nous considérons la
prestation comme annulée et vous ne pourrez prétendre à aucun remboursement.
Si l’attente se prolonge au-delà d’une heure, et ceci sans avertissement préalable, le rendez-vous est considéré
comme annulé. La facturation reste néanmoins effective.

&nbsp;
<ol start="5">
 	<li><strong>Annulation de rdv</strong></li>
</ol>
Tout soin décommandé doit être annulé 48h à l’avance. A défaut, le règlement total sera dû ou défalqué du
forfait ou de la cure.

&nbsp;
<ol start="6">
 	<li><strong>Abonnements et cures</strong></li>
</ol>
Nos abonnements et cures sont réglables d’avances.
Les soins non-consommés ne sont ni remboursables, ni échangeables.

&nbsp;
<ol start="7">
 	<li><strong>Bon cadeau</strong></li>
</ol>
Les personnes bénéficiant de bon cadeau doivent présenter ce dernier le jour du soin dans le cas contraire la
prestation lui sera facturée, les bons cadeaux ne sont ni remboursables, ni échangeables, les bons cadeaux sont
valables 3 mois à partir de la date d’achat et doivent être payés en totalité.

&nbsp;
<ol start="8">
 	<li><strong>Droit de rétractation</strong></li>
</ol>
L’institut crie institut accorde au client un délai de rétractation de 7 jours calendaires pour annuler une
réservation de soin esthétique (cure, forfait, bons cadeaux) sauf si la prestation à été réalisée pendant ce délai de
7 jours.
Ce délai court à compter du jour de l’achat du soin esthétique (cure, forfait, bons cadeaux)
Toute rétractation doit faire l’objet d’un courrier recommandé envoyé à : Cryo Institut – 8 rue Paul Lafargue
69100 Villeurbanne en rappelant votre type de réservation.

&nbsp;
<ol start="9">
 	<li><strong>Objets personnels</strong></li>
</ol>
Cryo Institut décline toute responsabilité de perte ou vol d’effets personnels dans l’enceinte des lieux.
Nous vous recommandons de prendre soin de vos objets de valeur, l’établissement ne pourra être tenu
responsable des objets oubliés.

&nbsp;
<ol start="10">
 	<li><strong>Service clientèle</strong></li>
</ol>
Pour toute information ou question, un numéro de téléphone est à votre disposition : 04 37 23 97 78.

&nbsp;
<ol start="11">
 	<li><strong>Droit applicable litiges</strong></li>
</ol>
Le présent contrat est soumis à la loi française.
La langue du présent contrat est la langue française.

[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:21:"Règlement Intérieur";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:19:"reglement-interieur";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-02-10 12:53:54";s:17:"post_modified_gmt";s:19:"2019-02-10 11:53:54";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:37:"https://cryoinstitut.fr/?page_id=9359";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}