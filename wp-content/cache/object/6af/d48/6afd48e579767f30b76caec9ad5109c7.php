��\<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7849;s:11:"post_author";s:1:"3";s:9:"post_date";s:19:"2018-12-12 16:05:02";s:13:"post_date_gmt";s:19:"2018-12-12 15:05:02";s:12:"post_content";s:5941:"[vc_row full_height="yes" parallax="content-moving" content_full_width="1" enable_parallax="1" css=".vc_custom_1544699763372{background-image: url(https://cryoinstitut.fr/wp-content/uploads/2018/12/RADIOFREQUENCE-limoges.png?id=8081) !important;background-position: center;background-repeat: no-repeat !important;background-size: cover !important;}" el_class="ro-service-spa"][vc_column][vc_empty_space height="153"][vc_custom_heading text="Radio Fréquence Visage" font_container="tag:h1|text_align:center|color:%231e73be"][vc_empty_space height="140"][/vc_column][/vc_row][vc_row][vc_column][vc_empty_space][vc_column_text]
<h2 style="text-align: center;"><span style="color: #0000ff;">RADIO FRÉQUENCE VISAGE ET CORPS À LYON (69), CRYO INSTITUT</span></h2>
[/vc_column_text][vc_single_image image="8395" img_size="full" alignment="center"][/vc_column][/vc_row][vc_row css=".vc_custom_1544700562548{margin-top: 30px !important;}"][vc_column][vc_video link="https://www.youtube.com/watch?v=MMtJoRmY36s" el_width="60" align="center"][/vc_column][/vc_row][vc_row][vc_column width="1/2"][vc_column_text]
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<h3><span style="color: #808080;">RADIOFRÉQUENCE</span></h3>
</div>
</div>
<div class="vc_empty_space"></div>
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">

<span style="color: #808080;">Par l’onde diathermique il est possible de restaurer un système adéquat de vascularisation de toutes les zones traitées et de stimuler certains processus naturels, tels que la production de collagène, d’élastine et d’acide hyaluronique, en augmentant leur action.</span><a href="https://cryoinstitut.fr/wp-content/uploads/2018/12/visage.png"><img class="alignnone wp-image-8099" src="https://cryoinstitut.fr/wp-content/uploads/2018/12/visage-300x130.png" alt="" width="411" height="178" /></a>

</div>
<a href="https://cryoinstitut.fr/wp-content/uploads/2018/12/visage2.png"><img class="alignnone wp-image-8100" src="https://cryoinstitut.fr/wp-content/uploads/2018/12/visage2-300x128.png" alt="" width="403" height="172" /></a>

</div>
[/vc_column_text][/vc_column][vc_column width="1/2"][vc_column_text]
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<h3><span style="color: #808080;">LIFTING</span></h3>
</div>
</div>
<div class="vc_empty_space"></div>
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">

<span style="color: #808080;">Ces courants spécifiques essaient de stimuler les influx nerveux physi­ologiques dans le but de restaurer les tissus. Le lifting électrique est à même de produire une stimulation sur la micro circulation cutanée, qui à son tour, favorise la nutrition et l’oxygénation des tissus tout en favorisant la revitali­sation. Il stimule également les ” fibroblastes » et augmente la quantité et la qualité du collagène synthétisé par ces derniers et favorise le système lymphatique et ses fonctions. Le traitement est divisé en quatre phases et chacune utilise un type de courant différent.</span>

<a href="https://cryoinstitut.fr/wp-content/uploads/2018/12/visage3-1.png"><img class="alignnone wp-image-8106" src="https://cryoinstitut.fr/wp-content/uploads/2018/12/visage3-1.png" alt="" width="419" height="180" /></a>

</div>
</div>
[/vc_column_text][/vc_column][vc_column][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<h3 style="text-align: center;">RADIOFRÉQUENCE BIPOLAIRE</h3>
</div>
</div>
<div class="vc_empty_space" style="text-align: center;"></div>
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<p style="text-align: left;">En luttant contre au relâchement de la peau dû au temps, le traitement entraîne la contraction de la peau du visage, avec un effet liftant (rajeu­nissement du visage) visible surtout après quelques mois. En augmentant la température du tissu conjonctif existant et, en incitant par la chaleur les fibroblastes à produire du nouveau collagène et une nouvelle élastine, il permet de réduire les rides et les signes de vieillissement, en offrant à la peau un effet lifting tout à fait naturel.</p>
<p style="text-align: center;"><a href="https://cryoinstitut.fr/wp-content/uploads/2018/12/visage2.png"><img class="alignnone size-full wp-image-8100" src="https://cryoinstitut.fr/wp-content/uploads/2018/12/visage2.png" alt="" width="537" height="230" /></a></p>

</div>
</div>
[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<h3 style="text-align: center;">RADIOFRÉQUENCE MULTIPOLAIRE</h3>
</div>
</div>
<div class="vc_empty_space" style="text-align: center;"></div>
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<p style="text-align: center;">Elle est utilisée pour obtenir le rajeunissement de la peau, grâce à l’au­gmentation de la température sous-cutanée, qui provoque une dénatura­tion des protéines du collagène, qui se rétrécissent et s’épaississent (effet lifting), ce qui conduit à une augmentation de la consistance du derme. La lésion thermique provoquée par la radiofréquence active également les fibroblastes avec une augmentation conséquente de la durée du collagène, de l’élastine, ainsi qu’une augmentation de la densité dermique, de qui pro­voque un plus grand étirement des tissus.</p>

</div>
<p style="text-align: center;"><a href="https://cryoinstitut.fr/wp-content/uploads/2018/12/visage4.png"><img class="alignnone wp-image-8119" src="https://cryoinstitut.fr/wp-content/uploads/2018/12/visage4.png" alt="" width="492" height="210" /></a></p>

</div>
[/vc_column_text][vc_empty_space][/vc_column][/vc_row]";s:10:"post_title";s:17:"Traitement Visage";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:17:"traitement-visage";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-03-25 10:08:14";s:17:"post_modified_gmt";s:19:"2019-03-25 09:08:14";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:37:"https://cryoinstitut.fr/?page_id=7849";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}