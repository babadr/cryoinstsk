H�\<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7824;s:11:"post_author";s:1:"3";s:9:"post_date";s:19:"2018-12-12 15:53:13";s:13:"post_date_gmt";s:19:"2018-12-12 14:53:13";s:12:"post_content";s:4599:"[vc_row full_height="yes" parallax="content-moving" content_full_width="1" enable_parallax="1" css=".vc_custom_1544699511965{border-bottom-width: 10px !important;background-image: url(https://cryoinstitut.fr/wp-content/uploads/2018/12/radio-frequence-lyon-villeurbanne.jpg?id=8055) !important;background-position: center;background-repeat: no-repeat !important;background-size: contain !important;}" el_class="ro-service-spa"][vc_column][vc_empty_space height="153"][vc_custom_heading text="LA RADIOFRÉQUENCE : RETENDRE PEAU VENTRE SANS CHIRURGIE" font_container="tag:h1|text_align:center|color:%23002c7a"][vc_empty_space height="140"][/vc_column][/vc_row][vc_row][vc_column width="2/3" css=".vc_custom_1544786083891{padding-right: 2% !important;padding-left: 2% !important;}"][vc_empty_space height="42px"][vc_column_text]
<div class=" et_pb_row et_pb_row_0">
<div class="et_pb_column et_pb_column_1_2 et_pb_column_0">
<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_text_0">
<div class=" et_pb_row et_pb_row_0">
<div class="et_pb_column et_pb_column_1_2 et_pb_column_0">
<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_text_0">
<h2><span style="color: #000000;">Qu’est Ce Que La Radiofréquence ?</span></h2>
<span style="color: #808080;">Il s’agit d’une technique destiné à retendre la peau relâchée du visage (perte de l’ovale du visage) ou du corps (bras, cuisses, ventre, ovale du visage, bajoues, cou…).</span>
<h4><span style="color: #000000;">Comment Ça Marche ?</span></h4>
<span style="color: #808080;">Basée sur l’émission d’ondes électromagnétiques à très haute fréquence, elle passent à travers la peau et produisent de la chaleur dans les tissus sous-cutanés.</span>

<span style="color: #808080;">La production de chaleur par radiofréquence induit une rétraction et une production du collagène , ainsi elle améliore la fermeté et la tension cutanée.</span>

<span style="color: #808080;">La radiofréquence est surtout utile pour lutter contre le relâchement de la peau (ovale du visage, cou, aspect fripé des joues et des pattes d’oie, ventre, bras…).</span>
<h4><span style="color: #000000;">La Méthode</span></h4>
</div>
</div>
</div>
<div class=" et_pb_row et_pb_row_1">
<div class="et_pb_column et_pb_column_1_2 et_pb_column_2">
<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_text_1">

<span style="color: #808080;">L’application des ondes de très haute fréquence est ressentie comme une chaleur à chaque tir. Toute la zone de peau que l’on veut retendre est traitée. Sensibles mais supportables, les séances sont espacées de 3 semaines. Une application préalable de crème anesthésiante ou l’insensibilisation par de l’air froid sont possibles.</span>
<h4><span style="color: #000000;">Les Avantages De La Radiofréquence</span></h4>
<span style="color: #808080;">Cette méthode moderne par radiofréquence est très efficace :</span>
<ol>
 	<li><span style="color: #000000;"><strong>La séance se termine sans marque. </strong></span></li>
 	<li><span style="color: #000000;"><strong>C’est indolore.</strong></span></li>
 	<li><span style="color: #000000;"><strong>On obtient un aspect lissé.</strong></span></li>
 	<li><span style="color: #000000;"><strong>Une amélioration de l’élasticité et de la tonicité.</strong></span></li>
 	<li><span style="color: #000000;"><strong>Lutte contre le relâchement de la peau sans chirurgie.</strong></span></li>
</ol>
<span style="color: #808080;">En général, les résultats s’améliorent pendant encore quelques mois après la fin du programme. Ils sont durables dans le temps.</span>

</div>
</div>
</div>
</div>
</div>
</div>
[/vc_column_text][/vc_column][vc_column width="1/3"][vc_empty_space height="62px"][contact-form-7 id="8337"][/vc_column][/vc_row][vc_row content_full_width="1"][vc_column][vc_gmaps link="#E-8_JTNDaWZyYW1lJTIwc3JjJTNEJTIyaHR0cHMlM0ElMkYlMkZ3d3cuZ29vZ2xlLmNvbSUyRm1hcHMlMkZlbWJlZCUzRnBiJTNEJTIxMW0xNCUyMTFtOCUyMTFtMyUyMTFkNTU2Ni4yOTQ5MTQ1ODU1MzUlMjEyZDQuODgzNDclMjEzZDQ1Ljc2ODIzNSUyMTNtMiUyMTFpMTAyNCUyMTJpNzY4JTIxNGYxMy4xJTIxM20zJTIxMW0yJTIxMXMweDAlMjUzQTB4NzA5NGNlZDZiNjQzZWI4OCUyMTJzVmlsbGUlMkJkZSUyQlZpbGxldXJiYW5uZSUyMTVlMCUyMTNtMiUyMTFzZnIlMjEyc2luJTIxNHYxNTQ0NjMxMTM0NDQ1JTIyJTIwd2lkdGglM0QlMjIxMDAlMjUlMjIlMjBoZWlnaHQlM0QlMjI0NTAlMjIlMjBmcmFtZWJvcmRlciUzRCUyMjAlMjIlMjBzdHlsZSUzRCUyMmJvcmRlciUzQTAlMjIlMjBhbGxvd2Z1bGxzY3JlZW4lM0UlM0MlMkZpZnJhbWUlM0U="][/vc_column][/vc_row]";s:10:"post_title";s:18:"La radiofréquence";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:17:"la-radiofrequence";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-03-25 09:48:23";s:17:"post_modified_gmt";s:19:"2019-03-25 08:48:23";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:37:"https://cryoinstitut.fr/?page_id=7824";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}